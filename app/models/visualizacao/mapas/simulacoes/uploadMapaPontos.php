<?php
	include_once("../../../conecta.php");
	
	header("Content-Type: text/plain");
	//ini_set('display_errors', 0);
	//ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	ini_set('memory_limit', '1024M');
	ini_set('max_execution_time', 0);

	$arquivo = $_FILES['file']['name'];

	$nome = substr($arquivo , 0 , stripos($arquivo, "."));

	$handle = fopen($_FILES['file']['tmp_name'], "r");
	$first = ftell($handle);
	
	$line = fgetcsv($handle,3500,";");

	$nciclos = count($line);
	//fseek($handle, $first, SEEK_SET);

    $sql = "CREATE TABLE $nome(";
    
	for($i = 0 ; $i < $nciclos ; $i++){
        if($line[$i] == "id" || $line[$i] == "x" || $line[$i] == "y" || $line[$i] == "X" || $line[$i] == "Y" || $line[$i] == "ciclo" || $line[$i] == "CICLO"){
            $sql .= " ".$line[$i]." int";
        }
        else if($line[$i] == "geom" || $line[$i] == "GEOM"){
            $sql .= " ".$line[$i]." geom";
        }
        else{
            $sql .= " ".$line[$i]." text";
        }
		if($i < $nciclos -1)
			$sql .= ",";
	}
	$sql .= ");";
	$tst = 0;
    $qry = pg_query($conn,$sql);
	if($qry == false)
		$recall= "duplicado";
	else
	{
		while ($line = fgetcsv($handle,3500,";")) {
            $tst++;
			$sql = "INSERT INTO $nome VALUES (";
			for($i = 0 ; $i < $nciclos ; $i++){
				$sql .= "'".$line[$i]."'";
				if($i < $nciclos-1)
					$sql .= ",";
			}
            $sql .= ");";
			$qry = pg_query($conn,$sql);
		}
		$recall = "success";
	}
	echo json_encode($recall);
    fclose($handle);
    
?>
