<?php
	include_once("../../../conecta-simula.php");
	
	header("Content-Type: text/plain");
	//ini_set('display_errors', 0);
	//ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	ini_set('memory_limit', '1024M');
	ini_set('max_execution_time', 0);

	$arquivo = $_FILES['file']['name'];

	$nome = substr($arquivo , 0 , stripos($arquivo, "."));

	$handle = fopen($_FILES['file']['tmp_name'], "r");
	$first = ftell($handle);
	
	$line = fgetcsv($handle,3500,";");

	$nciclos = count($line)-2;
	fseek($handle, $first, SEEK_SET);

	$sql = "CREATE TABLE $nome(x int NOT NULL, y int NOT NULL,";
	for($i = 0 ; $i < $nciclos ; $i++){
		$sql .= "c$i int";
		if($i < $nciclos -1)
			$sql .= ",";
	}
	$sql .= ");";
	$tst = 0;
	$qry = pg_query($conn_simula,$sql);

	if($qry == false)
		$recall= "duplicado";
	else
	{
		while ($line = fgetcsv($handle,3500,";")) {
			$tst++;
			$sql = "INSERT INTO $nome VALUES (";
			for($i = 0 ; $i < $nciclos ; $i++){
				$sql .= "'".$line[$i]."'";
				if($i < $nciclos-1)
					$sql .= ",";
			}
			$sql .= ");";
			$qry = pg_query($conn_simula,$sql);
		}
		$recall = "success";
	}
	echo json_encode($recall);
	fclose($handle);
?>
