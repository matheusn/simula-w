<?php
/*
*   Retorna a coordenada dos pontos solicitados
*
*   21/05/2018
*/
    function LoadCoordenadaPontos($layer, $conn, $geom){
        header("Content-Type: text/plain");
        if($geom){
            $sql = "select regexp_replace(ST_Astext(ST_Transform( ST_SetSRID(geom,31982) , 3857)), '[A-Z()]', '', 'g') from $layer ";
            $qry = pg_query($conn,$sql);
            $nlinhas = pg_numrows($qry);
            if($nlinhas > 0){
                $dados = [];
                for($i = 0 ; $i < $nlinhas ; $i++){
                    $dados [] = pg_fetch_assoc($qry, $i);
                }
                echo json_encode($dados, JSON_PRETTY_PRINT);
            }
            else{
                echo "Erro na consulta";
            }
        }
        else{
            $sqlPontos = "select regexp_replace(ST_Astext(ST_Transform( ST_SetSRID(st_makepoint(x,y),31982) , 3857)), '[A-Z()]', '', 'g') from $layer";
            $queryPontos = pg_query($conn, $sqlPontos);
            $nlinhas = pg_numrows($queryPontos);
            if($nlinhas > 0){
                $dados = [];
                for($i = 0 ; $i < $nlinhas ; $i++){

                    $dados [] = pg_fetch_assoc($queryPontos, $i);
                }
                echo json_encode($dados, JSON_PRETTY_PRINT);
            }
            else{
                echo "Erro na consulta";
            }
        }
    }
?>