<?php
	include_once("../../conecta-simula.php");

	header("Content-Type: text/plain");
	ini_set('display_errors', 0);
	ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    //ini_set('memory_limit', '1024M');
    $config['temp_dir'] = RCUBE_INSTALL_PATH . 'temp/';

    $simulacao = $_POST['simulacao'];
    $arquivo = $_FILES['file']['name'];

    $recall = [];
    $recall[0] = $arquivo;
    $nome = substr($arquivo , 0 , stripos($arquivo, "."));
    $nome = $simulacao."_".$nome;
    $handle = fopen($_FILES['file']['tmp_name'], "r");
    $first = ftell($handle);
    $line = fgetcsv($handle,3500,";");
    $nciclos = count($line)-2;
    fseek($handle, $first, SEEK_SET);
    $nome = str_replace('-', '_', $nome);
    $sql = "select nome from tabelas_graficos where simulacao = '$simulacao' and nome = '$nome'";
    $sql = strtolower($sql);
    $qry = pg_query($conn_simula,$sql);

    if(pg_num_rows($qry)){
        $recall[1]= "duplicado";
    }
    else
    {
        $sql = "SELECT n_quadras from graficos_simulacao where nomes = '$simulacao'";
        $queryquadras = pg_query($conn_simula,$sql);
        $numQuadras = pg_fetch_assoc($queryquadras);
        $update_n = $numQuadras['n_quadras']+1;
        $sql = "UPDATE graficos_simulacao SET n_quadras = '$update_n' WHERE nomes = '$simulacao'";
        $queryquadras = pg_query($conn_simula,$sql);
        $sql = "insert into tabelas_graficos values ('$simulacao' ,'$nome')";
        $sql = strtolower($sql);
        $qry = pg_query($conn_simula,$sql);
        $sql = "CREATE TABLE $nome (";
        for($i = 0 ; $i < $nciclos ; $i++){
            $sql .= "a$i int";
            if($i < $nciclos -1)
                $sql .= ",";
        }
        $sql .= ");";
        $sql = strtoupper($sql);
        $qry = pg_query($conn_simula,$sql);
        while ($line = fgetcsv($handle,3500,";")) {
            $sql = "INSERT INTO $nome VALUES (";
            for($i = 0 ; $i < $nciclos ; $i++){
                $sql .= "'".$line[$i]."'";
                if($i < $nciclos-1)
                    $sql .= ",";
            }
            $sql .= ");";
            $sql = strtolower($sql);
            $qry = pg_query($conn_simula,$sql);
        }
        $recall[1] = "success";
    }
    echo json_encode($recall);
    fclose($handle);
?>
