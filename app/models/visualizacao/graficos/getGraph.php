<?php
    header("Content-Type: text/plain");

    ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_DEPRECATED);
    ini_set('memory_limit', '1024M');
    
    include_once("../../utils/funcoes.php");
    include_once("../../conecta-simula.php");
    include_once('humanos.php');
    include_once('mosquito_d.php');
    include_once('mosquito_w.php');

    $simulacao = $_POST['simulacao'];
    $individuo = $_POST['individuo'];
    $quadra    = $_POST['quadra'];
    $grafico   = $_POST['grafico'];

    //monta o nome da tabela, por quadra ou tabela de total
    if($quadra == "total")
        $nome_grafico = "quantidades_".$individuo."_total";
    else
        $nome_grafico = "quantidades_".$individuo."_quadra_".$quadra;
    $nome_grafico = $simulacao."_".$nome_grafico; 

    //verifica se a tabela existe
    $sql = "select nome from tabelas_graficos where simulacao = '$simulacao' and nome = '$nome_grafico'";
    $sql = strtolower($sql);
    $qry = pg_query($conn_simula,$sql);

    //caso a tabela exista obtem os dados desta
    if($qry)
    {
        //obtem dados da tabela
        $sql = "select * from $nome_grafico";
        $sql = strtoupper($sql);
        $qry = pg_query($conn_simula,$sql);
        $ntables = pg_numrows($qry);
        $tabela;
        for( $i = 0 ; $i < $ntables ; $i++)
            $tabela[$i] = pg_fetch_array($qry, $i);
        
        //instancia a tabela do individuo e executa a função referente ao grafico solicitado
        if($individuo == "humanos"){
            $GraficoHumanos = new GraficosQuantidadesHumanos($tabela);
            $grafico = $GraficoHumanos->{$grafico}();
        }
        else if($individuo == "mosquitos_dengue"){
            $GraficoMDenge = new GraficosQuantidadesMosquitosDengue($tabela);
            $grafico = $GraficoMDenge->{$grafico}();
        }
        else{
            $GraficoMWolbachia = new GraficosQuantidadesMosquitosWolbachia($tabela);
            $grafico = $GraficoMWolbachia->{$grafico}();
        }

        //retorna o array com as informações do grafico
        echo json_encode($grafico , JSON_PRETTY_PRINT);
    }
?>
