<?php
/*
    Matheus N Ismael 21/02/19
*/
    header('Content-Type: text/plain');
    include_once('../conecta.php');
    include_once('../conecta-simula.php');
    include_once('TabelasEntradasSimulacao.php');
    include_once('TabelaEntradasAmbientais.php');

    $entrada = isset($_POST['entrada'])? $_POST['entrada'] : null;
    $base    = $_POST['base'];
    $tabela  = $_POST['tabela'];

    $Tabela;
    $dados;

    if($tabela == "ambientais")
        $dados = new TabelaEntradasAmbientais($base, $conn_simula, null);
    else
        $dados = new TabelaEntradasSimulacao($entrada, $base, $conn_simula, $conn_simula, null);

    $Tabela = $dados->{"retornaTabela"}();

    echo $Tabela;
?>


