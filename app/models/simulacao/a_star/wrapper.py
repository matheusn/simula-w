#!/usr/bin/env python3

import ctypes as ct
import numpy as np
import os

# Importa a biblioteca externa C para execução
cwd = os.getcwd()
_, tail = os.path.split(cwd)
if tail == 'GeradorPython':
	os.chdir(os.path.join(cwd, 'a_star'))
os.system('bash compile.sh')  # Compila a biblioteca
lib = ct.cdll.LoadLibrary('./a_star/a_star.so')
os.chdir(cwd)

# Define tipos para os argumentos e tipos de retorno para as funções
ptr = ct.c_void_p
lib.AStar_new.restype = ptr
lib.AStar_delete.argtypes = [ptr]
lib.AStar_setVerts.argtypes = [ptr, ct.c_int, ptr]
lib.AStar_setRotas.argtypes = [ptr, ct.c_int, ptr]
lib.AStar_setOrigem.argtypes = [ptr, ptr, ptr]
lib.AStar_setDestino.argtypes = [ptr, ptr, ptr]
lib.AStar_setQuadrasLotes.argtypes = [ptr, ptr, ptr, ptr, ptr]
lib.AStar_setEsquinas.argtypes = [ptr, ct.c_int, ptr, ptr, ptr, ptr]
lib.AStar_setRuasDestino.argtypes = [ptr, ct.c_int, ptr, ptr, ptr]
lib.AStar_proc.argtypes = [ptr]
lib.AStar_saidaIndexRotas.argtypes = [ptr, ptr]
lib.AStar_saidaVetorRotas.argtypes = [ptr, ptr]

class AStar:

	def __init__(self):
		self.obj = lib.AStar_new()

	def __del__(self):
		lib.AStar_delete(self.obj)

	def set_verts(self, verts):
		n_verts = len(verts)
		id_lotes_arr = verts.id_lote.values.astype('int32')
		id_lotes = id_lotes_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setVerts(self.obj, n_verts, id_lotes)

	def set_rotas(self, rotas):
		# Define o total de rotas e os identificadores da primeira rua
		n_rotas = len(rotas)
		p_ruas_arr = rotas.p_rua.values.astype('int32')
		p_ruas = p_ruas_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setRotas(self.obj, n_rotas, p_ruas)
		# Define as coordenadas do centroide do lote de origem
		x1_arr = rotas.x1.values.astype('int32')
		y1_arr = rotas.y1.values.astype('int32')
		x1 = x1_arr.ctypes.data_as(ct.c_void_p)
		y1 = y1_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setOrigem(self.obj, x1, y1)
		# Define as coordenadas do centroide do lote de destino
		x2_arr = rotas.x2.values.astype('int32')
		y2_arr = rotas.y2.values.astype('int32')
		x2 = x2_arr.ctypes.data_as(ct.c_void_p)
		y2 = y2_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setDestino(self.obj, x2, y2)
		# Define os identificadores de quadra e lote
		qa_arr = rotas.q1_id.values.astype('int32')  # Quadra de origem
		la_arr = rotas.l1_id.values.astype('int32')  # Lote de origem
		qb_arr = rotas.q2_id.values.astype('int32')  # Quadra de destino
		lb_arr = rotas.l2_id.values.astype('int32')  # Lote de destino
		qa = qa_arr.ctypes.data_as(ct.c_void_p)
		la = la_arr.ctypes.data_as(ct.c_void_p)
		qb = qb_arr.ctypes.data_as(ct.c_void_p)
		lb = lb_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setQuadrasLotes(self.obj, qa, la, qb, lb)

	def set_esquinas(self, centr):
		n_esquinas = len(centr)
		ra_arr = centr.id_l1.values.astype('int32')
		rb_arr = centr.id_l2.values.astype('int32')
		x_arr = centr.x.values.astype('int32')
		y_arr = centr.y.values.astype('int32')
		ra = ra_arr.ctypes.data_as(ct.c_void_p)
		rb = rb_arr.ctypes.data_as(ct.c_void_p)
		x = x_arr.ctypes.data_as(ct.c_void_p)
		y = y_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setEsquinas(self.obj, n_esquinas, ra, rb, x, y)

	def set_ruas_destino(self, ruas_destino):
		n_rows = len(ruas_destino)
		quadras_arr = ruas_destino.id_quadra.values.astype('int32')
		lotes_arr = ruas_destino.id_lote.values.astype('int32')
		ruas_arr = ruas_destino.id_rua.values.astype('int32')
		quadras = quadras_arr.ctypes.data_as(ct.c_void_p)
		lotes = lotes_arr.ctypes.data_as(ct.c_void_p)
		ruas = ruas_arr.ctypes.data_as(ct.c_void_p)
		lib.AStar_setRuasDestino(self.obj, n_rows, quadras, lotes, ruas)

	def proc(self):
		lib.AStar_proc(self.obj)

	def output(self, n_rotas):
		# Obtém o índice por rotas
		index_rotas = np.zeros(n_rotas + 1, dtype=np.int32)
		index_ptr = index_rotas.ctypes.data_as(ct.c_void_p)
		lib.AStar_saidaIndexRotas(self.obj, index_ptr)
		# Obtém o vetor de rotas
		size = index_rotas[-1]  # O último valor será o tamanho do vetor
		vetor_rotas = np.zeros(size, dtype=np.int32)
		vetor_ptr = vetor_rotas.ctypes.data_as(ct.c_void_p)
		lib.AStar_saidaVetorRotas(self.obj, vetor_ptr)
		# Retorna as duas arrays
		return index_rotas, vetor_rotas
