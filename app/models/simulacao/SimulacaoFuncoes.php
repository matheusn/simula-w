<?php
/*
*   Matheus N Ismael 15/04/19
*
*/

function retornaEmExecucao($conn){
    $dados = [];
    $nlinhas;
    $sqlSimulacoes   = "SELECT * FROM simulacoes ORDER BY nome";
    try{
        $querySimulacoes = pg_query($conn, $sqlSimulacoes);
        if($nlinhas = pg_numrows($querySimulacoes))
            for($i = 0 ; $i < $nlinhas ; $i++)
                $dados[] = pg_fetch_array($querySimulacoes, $i);
    }catch(Exception $e){

    }
    for($i = 0 ; $i < $nlinhas ; $i++){
        if($dados[$i][1] == "Processando")
            return dados[$i][1];
    }
    return null;
}

function updateStatus($simul, $status, $conn){
    $sqlAlteraStatus = "UPDATE simulacoes SET status = '$status' where nome = '$simul'";
    $success = true;
    try{
        $queryAlteraStatus = pg_query($conn, $sqlAlteraStatus);
    }catch(Exception $e){
        $success = false;
    }
    return $success;
}

function criarPastasParametros($dir){
    $pasta = $dir."Entradas/MonteCarlo_0";
    mkdir($pasta."/Ambiente", 0777, true);
    mkdir($pasta."/Humanos", 0777, true);
    mkdir($pasta."/Mosquitos", 0777, true);
    mkdir($pasta."/Simulacao", 0777, true);
}

function geraArquivosTabelas($simul, $conn_simula, $conn, $MosquitosTabelas, $HumanosTabelas, $SimulacaoTabelas, $dir){
    $success = true;
    try{   
        $tabelaMosquitos = new TabelaEntradasSimulacao("mosquitos", $simul, $conn_simula, $conn_simula, $MosquitosTabelas);
        $TabelaM = $tabelaMosquitos->{"geraArquivos"}($dir);

        $tabelaHumanos   = new TabelaEntradasSimulacao("humanos",   $simul, $conn_simula, $conn_simula, $HumanosTabelas);
        $TabelaH = $tabelaHumanos->{"geraArquivos"}($dir);

        $tabelaSimulacao = new TabelaEntradasSimulacao("simulacao", $simul, $conn_simula, $conn_simula, $SimulacaoTabelas);
        $TabelaS = $tabelaSimulacao->{"geraArquivos"}($dir);
    }catch(Exception $e){
        $success = false;
    }
    return $success;
}

function geraAmbientais($simul, $path){
    $success = true;
    try{ 
        $execut = "../../../apk/GeradorAmbientais/gerador.py ";
        $comando   = escapeshellcmd('python3 '.$execut.$simul.' '.$path.' &');
        $cmdResult = exec($comando);
        echo $cmdResult;
    }catch(Exception $e){
        $success = false;
    }
    return $success;
}

function executaSimulacao(){
    $success = true;
    try{ 
        $execut = "../../../apk/AEDES_Acoplado/AEDES_Acoplado";
        $comando   = escapeshellcmd('./'.$execut.' &');
        $cmdResult = exec($comando);
        echo $cmdResult;
    }catch(Exception $e){
        $success = false;
    }
    return $success;
}

function monitoraSimulacao($simul){
    $success = true;
    try{ 
        $execut = "./../../../apk/Monitora.sh ";
        $comando   = escapeshellcmd('bash '.$execut." AEDES_Acoplado");
        $cmdResult = exec($comando);
        echo $cmdResult;
    }catch(Exception $e){
        $success = false;
    }
    return $success;
}

?>
