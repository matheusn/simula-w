<?php
/*
    Matheus N Ismael 13/02/19
*/
    header('Content-Type: text/plain');
    include_once('../conecta-simula.php');

    $dados = [];

    $sqlSimulacoes   = "SELECT * FROM simulacoes";
    $querySimulacoes = pg_query($conn_simula, $sqlSimulacoes);
    if($nlinhas = pg_numrows($querySimulacoes))
        for($i = 0 ; $i < $nlinhas ; $i++)
            $dados[] = pg_fetch_assoc($querySimulacoes, $i);


    echo json_encode($dados, JSON_PRETTY_PRINT);
?>