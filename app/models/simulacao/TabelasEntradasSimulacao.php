<?php

/*
*   Matheus N Ismael 27/02/19
*
*/

class TabelaEntradasSimulacao{
    private static $tabelas = array();
    private static $simulacaoBase;
    private static $entrada;
    private static $conn_dados;
    private static $conn_legendas;
    private static $dados;
    private static $legendas;
    private static $Tabela;

    public function __construct($entrada, $simulacaoBase, $conn_dados, $conn_legendas, $dados){
        self::$tabelas["humanos"]     = [];
        self::$tabelas["mosquitos"]   = [];
        self::$tabelas["simulacao"]   = [];

        self::$tabelas["humanos"]     = ["0-INI", "1-MOV", "2-CON", "3-TRA", "4-CON", "5-INS"];
        self::$tabelas["mosquitos"]   = ["0-INI", "1-MOV", "2-CON", "3-TRA", "4-CON", "5-GER"];
        self::$tabelas["simulacao"]   = ["0-SIM"];

        self::$simulacaoBase = $simulacaoBase;
        self::$entrada       = $entrada;
        self::$conn_dados    = $conn_dados;
        self::$conn_legendas = $conn_legendas;
        self::$dados         = $dados;
        self::$legendas      = [];
        self::$Tabela        = [];
    } 

    function obtemLegendas($base, $entrada, $tabela){
        $legendas      = [];
        $sqlLegendas   = "SELECT descricao FROM entradas_simulacao WHERE entrada = '$entrada' AND tabela = '$tabela' order by tabela;";
        $queryLegendas = pg_query(self::$conn_legendas, $sqlLegendas);

        if($nlinhas = pg_numrows($queryLegendas))
            for($i = 0 ; $i < $nlinhas ; $i++)
                $legendas[] = pg_fetch_array($queryLegendas, $i);
        return $legendas;
    }

    function obtemDados($base, $entrada, $tabela){
        $dados       = [];
        $sqlDados    = "SELECT codigo, min, max FROM $base WHERE entrada = '$entrada' AND arquivo = '$tabela' order by arquivo;";
        $queryDados  = pg_query(self::$conn_dados, $sqlDados);

        if($nlinhas = pg_numrows($queryDados))
            for($i = 0 ; $i < $nlinhas ; $i++)
                $dados[] = pg_fetch_array($queryDados, $i);        
        return $dados;
    }
    
    function geraArquivos($caminho){ 
        $pasta = $caminho."Entradas/MonteCarlo_0";
        for($i = 0 ; $i < sizeof(self::$tabelas[self::$entrada]) ; $i++){
            $file = fopen($pasta."/".ucfirst(self::$entrada)."/".self::$tabelas[self::$entrada][$i].".csv", 'a');
            for($j = 0 ; $j < sizeof(self::$dados[self::$tabelas[self::$entrada][$i]]) ; $j++){
                $atual = self::$dados[self::$tabelas[self::$entrada][$i]][$j];
                fwrite($file, $atual['codigo'].";".$atual['min'].";".$atual['max'].";".$atual['descricao']."\n");
            }
        }   
    }

    public function salvaTabela(){
        $success = true;
        for($i = 0 ; $i < sizeof(self::$tabelas[self::$entrada]) ; $i++){
            for($j = 0 ; $j < sizeof(self::$dados[self::$tabelas[self::$entrada][$i]]) ; $j++ ){
                $regAtual = self::$dados[self::$tabelas[self::$entrada][$i]][$j];
                $sqlSave = "INSERT INTO ".self::$simulacaoBase." VALUES ('".self::$tabelas[self::$entrada][$i]."','".self::$entrada."','".$regAtual['codigo']."','".$regAtual['min']."','".$regAtual['max']."')";
                $querySave = pg_query(self::$conn_dados, $sqlSave);
                if(!$querySave)
                    $success = false;
            } 
        }
        if($success)
            return "salvo";
        return "erro";       
    }

    function retornaTabela(){
        $arquivos = [];
        $arquivos = self::$tabelas[self::$entrada];

        for($i = 0 ; $i < sizeof($arquivos) ; $i++){
            $dados     = $this->obtemDados(self::$simulacaoBase, self::$entrada, $arquivos[$i]);
            $legendas  = $this->obtemLegendas(self::$simulacaoBase  , self::$entrada , $arquivos[$i]);
            $tabela    = [];

            for($j = 0 ; $j < sizeof($dados) ; $j++){
                $parametro              = [];
                $parametro["codigo"]    = $dados[$j][0];
                $parametro["min"]       = $dados[$j][1];
                $parametro["max"]       = $dados[$j][2];
                $parametro["descricao"] = $legendas[$j][0];

                array_push($tabela, $parametro);
            }
            self::$Tabela[$arquivos[$i]] = $tabela;
        }
        return (json_encode(self::$Tabela, JSON_PRETTY_PRINT));
    }
}
?>
