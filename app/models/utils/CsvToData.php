<?php
header("Content-Type: text/plain");
ini_set('display_errors', 0);
ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_DEPRECATED);
$config['temp_dir'] = RCUBE_INSTALL_PATH . 'temp/';

$arquivo = $_FILES['file']['tmp_name'];

$dados = [];
$dados["nome"] = $_FILES['file'];
$dados["tabela"] = [];

$handle = fopen($arquivo, "r");
$legenda = fgetcsv($handle,3500,";");
$nciclos = count($line);

while ($line = fgetcsv($handle,3500,";")) {
    $temp = [];
    for($i = 0 ; $i < sizeof($legenda) ; $i++)
        $temp[strtolower($legenda[$i])] = $line[$i];
    array_push($dados["tabela"], $temp);
}
echo json_encode($dados);
fclose($handle);
?>