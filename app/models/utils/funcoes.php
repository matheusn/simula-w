<?php

function copia_m_v_sum($m, $ini , $fim , $inter){
    $v;

    for($j = 0 ; $j < count($m[0]) ; $j++){
        if($m[0][$j] == "")
            break;
        $v[$j] = 0;
    }
    $x = 0;
    for($i = 0 ; $i < count($m) ; $i++){
        for($j = $ini ; $j < $fim ; $j += $inter){
            $v[$i] += $m[$i][$j];
        }
    }
    return $v;
}
?>
