<?php
    header("Content-Type: text/plain");
    include_once("individualFuncoes.php");
    include_once("../conecta-simula.php");

    $agente = $_POST['agente'];
    $atributos = retornaAtributos($agente, $conn_simula, true);
    $dados = [];

    for($i = 0; $i < sizeof($atributos); $i++){
        $dados[$i]['pref'] = $atributos[$i]['pref'];
        $dados[$i]['atributo'] = $atributos[$i]['atributo'];
        
        //$dados['valores'] = [];
        $dados[$i]['valores'] = explode(",", $atributos[$i]['valores']);
    }
    echo json_encode($dados, JSON_PRETTY_PRINT);
?>