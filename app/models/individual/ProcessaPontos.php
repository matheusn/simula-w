<?php
    header("Content-Type: text/plain");
    include_once("individualFuncoes.php");
    include_once("../conecta-simula.php");

    $pontos = $_POST['pontos'];

    $pontos_p = [];

    for($i = 0; $i < sizeof($pontos); $i++){
        array_push($pontos_p, parseCoord($pontos[$i][0], $conn_simula));
    }

    echo json_encode($pontos_p, JSON_PRETTY_PRINT);
?>