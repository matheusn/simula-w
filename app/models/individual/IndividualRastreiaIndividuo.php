<?php
    $id = $_POST['id'];
    $agente_in = $_POST['agente'];
    $agente;
    $ciclo = $_POST['ciclo'];
    if($agente_in == "Humano")
        $agente = 'h';
    else 
        $agente = 'm';


    $jsonIn  = [];

    $jsonIn['id'] = $id;
    $jsonIn['ciclo'] = $ciclo;
    $jsonIn['agente'] = $agente;

    $json_rastreamento = json_encode($jsonIn, JSON_PRETTY_PRINT);

    $resultado;

    try{
        $fp = fopen("../../../apk/acompanhamento_Individual/entradaRastreamento.json", "w");
        $ff = fopen("../../../apk/acompanhamento_Individual/saidaRastreamento.json", "w");
        $escreve = fwrite($fp, $json_rastreamento);
        fclose($fp);
        fclose($ff);
        $resultado = "sucesso";
    }catch(Exception $e){
        echo json_encode("erro");
        $resultado = "erro";
    }
    if($resultado != "erro"){
        try{
            $execut = "../../../apk/acompanhamento_Individual/saidasbitstring/Rastreamento.py";
            $comando   = escapeshellcmd('python3 '.$execut);
            $cmdResult = exec($comando);
            $arquivo = file_get_contents('../../../apk/acompanhamento_Individual/saidaRastreamento.json');
            if($arquivo == "")
                echo json_encode("erro");
            else
                echo $arquivo;
        }
        catch(Exception $e){
            echo json_encode("erro");
        }
    }
    
?>