$(function() {
    $('.submit').on('click', function() {
        var file_data = $('.image').prop('files')[0];
        if (file_data != undefined) {
            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                type: 'POST',
                url: '../app/models/visualizacao/mapas/simulacoes/criaTabelaSimulacao.php',
                //url: '../app/models/visualizacao/mapas/simulacoes/uploadMapaPontos.php',
                contentType: false,
                processData: false,
                dataType: "json",
                data: form_data,
                enctype: "multipart/form-data",
                success: function(data) {
                    if (data == "success") {
                        alerta("Sucesso",'Arquivo carregado com sucesso.');
                        let listaVisualizacao = document.getElementById("mapas");
                        LoadMapsFromDb(listaVisualizacao);
                    } else if (data == "duplicado") {
                        alerta("Atenção",'O arquivo ja foi carregado anteriormente ou há algum problema na formatação do header<br><br>'+
                        "O header deve possuir valores unicos para cada coluna.");
                    } else {
                        alerta("Atenção",'Erro no carregamento. Tente novamente.');
                    }
                },
                error: function(errorThrown, data) {
                    alerta("Erro",'Houve um erro no servidor!\n Por favor relate isso');
                    console.log(errorThrown);
                    console.log(data);
                }
            });
        }
        return false;
    });
});


//https://gasparesganga.com/labs/php-shapefile/
