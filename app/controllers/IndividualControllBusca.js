//============================================================ componentes
var divTabelaFiltrosHumanos     = document.getElementById("divTabelaFiltrosHumanos");
var divTabelaAtributosHumanos   = document.getElementById("divTabelaAtributosHumanos");
var divTabelaFiltrosMosquitos   = document.getElementById("divTabelaFiltrosMosquitos");
var divTabelaAtributosMosquitos = document.getElementById("divTabelaAtributosMosquitos");

var divTabelaRes = document.getElementById("divTabelaRes");

var btnBuscarMosquitos = document.getElementById("btnBuscarMosquitos");
var btnBuscarHumanos   = document.getElementById("btnBuscarhumanos");

var inputCicloBuscarMosquitos = document.getElementById("inputCicloBuscarMosquitos");
var inputCicloBuscarHumanos   = document.getElementById("inputCicloBuscarHumanos");
//============================================================ dados
var atributosHumanos;
var atributosMosquitos;

var filtrosHumanos;
var filtrosMosquitos;

var tabelaAtributosHumanos;
var tabelaAtributosMosquitos;

var tabelaFiltrosHumanos;
var tabelaFiltrosMosquitos;

var mapLegendasAtributosMosquitos;
var mapLegendasAtributosHumanos;
//============================================================ tabelas

var tabelaRes;

function loadTabelaAtributos(div, dados){// carrega a tabela de atributos a serem exibidos
    return new Tabulator(div,{
        data: dados,
        height:"170px",
        width:"120px",
        resizableRows:true,
        columns:[
            {title:"", field:"visivel", headerSort:false, align:"center", width:30, formatter:"tickCross", sorter:"boolean", editor:true},
            {title:"", field:"atributo", headerSort:false, width: 273}
        ]
    });
}

function loadTabelaFiltros(div, dados){ // careega a tabela de filtros
    return new Tabulator(div,{
        data: dados,
        height:"160px",
        layout:"fitColumns", 
        width:"100px",
	    tooltips:true,
        history:true,
        resizableRows:false,
        columns:[
            {title:"Atributo", field:"atributo", headerSort:false ,width:213},
            {title:"Valores", field:"valor", headerSort:false, editor:"input" ,width: 100, align:"center", editorParams:"valores" }
        ]
    });
}

function loadTabelaResultados(dados, campos, agente_at){//carrega a tabela de resultados
    let colunas = [];
    let legendas;
    tabelaRes = null;
    if(agente_at == "Humanos")
        legendas = mapLegendasAtributosHumanos;
    else
        legendas = mapLegendasAtributosMosquitos;

    colunas.push({title:"gid", field:"gid", headerSort:false, width:100});

    for(var i = 0; i < campos.length; i++){
        colunas.push({
            title: legendas[campos[i]['pref']], 
            field: campos[i]['pref'], 
            headerSort:true ,
            width:150
        });
    }
    tabelaRes = new Tabulator(divTabelaRes,{
        data: dados,
        height:"600px",
        width:"100%",
        history:true,
        resizableRows:false,
        columns: colunas
    });
}

//============================================================ controll tabelas

function viewAtributos(){ // Chama as funções responsaveis por obter o conteudo das tabelas de 
                          // filtros e campos e exibi-las  
    atributosHumanos   = loadAtributosAgente("Humanos");
    atributosMosquitos = loadAtributosAgente("Mosquitos");

    filtrosHumanos   = loadAtributosAgenteFiltros("Humanos");
    filtrosMosquitos = loadAtributosAgenteFiltros("Mosquitos");

    tabelaAtributosHumanos   = loadTabelaAtributos(divTabelaAtributosHumanos, atributosHumanos);
    tabelaAtributosMosquitos = loadTabelaAtributos(divTabelaAtributosMosquitos, atributosMosquitos);   

    tabelaAtributosHumanos   = loadTabelaFiltros(divTabelaFiltrosHumanos, filtrosHumanos);
    tabelaAtributosMosquitos = loadTabelaFiltros(divTabelaFiltrosMosquitos, filtrosMosquitos);

    mapLegendasAtributosMosquitos = loadMapLegendasAtributos(atributosMosquitos);
    mapLegendasAtributosHumanos   = loadMapLegendasAtributos(atributosHumanos);
}

function dadosBusca(agente){ // obtem os parametros de busca inseridos
    var baseFiltros   = [];
    var baseAtributos = [];
    if(agente == "Humanos"){
        baseFiltros  = filtrosHumanos;
        baseAtributos = atributosHumanos;
    }
    else{
        baseFiltros   = filtrosMosquitos;
        baseAtributos = atributosMosquitos;
    }

    var filtros   = [];
    var atributos = [];
    for(var  i = 0; i < baseFiltros.length; i++){
        if(baseFiltros[i]['valor'] != null && baseFiltros[i]['valor'] != ""){
            filtros.push({"valor" : baseFiltros[i]['valor'], "pref" : baseFiltros[i]['pref']});
            atributos.push({"pref" : baseAtributos[i]['pref']});
        }
        else if(baseAtributos[i]['visivel'])
            atributos.push({"pref" : baseAtributos[i]['pref']});
    }
    let dadosBusca = {"filtros" : filtros, "atributos" : atributos};
    console.log(dadosBusca)
    return dadosBusca;
}

function requestBusca(agente, ciclo){ // requisita a busca 
    let dados = dadosBusca(agente);

    $.ajax({
        type : "POST",
        url : "../app/models/individual/IndividualBusca.php",
        dataType: "json",
        data : {'ciclo' : ciclo, 'agente' : agente, 'filtros' : dados.filtros, 'atributos' : dados.atributos},
        async : false,
        success: function(data){
            $(divTabelaRes).empty();
            let ndados = trataJsonResultado(data);
            loadTabelaResultados(ndados, dados.atributos, agente);   
        },
        error: function(){
            alerta('Erro','Individual. Erro ao Carregar!');
            console.log(errorThrown);
        }
    });
}

btnBuscarMosquitos.addEventListener("click", function(){ // evento busca mosquito
    let ciclo = inputCicloBuscarMosquitos.value;
    let busca = requestBusca("Mosquitos", ciclo);
});

btnBuscarHumanos.addEventListener("click", function(){ // evento busca humano
    let ciclo = inputCicloBuscarHumanos.value;
    let busca = requestBusca("Humanos", ciclo);    
});

