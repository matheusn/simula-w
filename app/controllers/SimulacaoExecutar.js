/*
    Matheus N Ismael 15/03/19
*/

var buttonExecutar = document.getElementById("botaoExecutar");

//================================================================

function executarSimulacao(){
    if(checaEmExecucao()){
        alerta("erro","Já existe uma simulação em execução.<br>Aguarde o fim do processamento.<br>Alterações nos registros não afetam a execução.")
    }
    else{
        alerta("Processando","A simulação está em execução!<br>Atualize o status para acompanhar o progresso.");
        var simulacaoAt = simulacaoAtual();
        simulacaoAt = simulacaoAt['nome'];

        //salvarAmbientais(simulacaoAt);
        //salvaArquivosTabelas(simulacaoAt);
        executarSimulacaoReq(simulacaoAt);
    }
}

function salvarAmbientais(base){ 
    $.ajax({
        type : "POST",
        url : "../app/models/simulacao/SalvaAmbientais.php",
        data : {'dados' : ConteudoAmbientais, 'base' : base},
        dataType : "json",
        async : false,
        success : function(data){
            if(data == "erro")
                alerta("Erro ao Salvar", "Simulação. Não foi possivel salvar a tabela de dados Ambientais");
            else
                alerta("Informações Salvas", "Simulação. Os dados de Informações ambientais foram salvos");
        },
        error : function(data){
            alerta("Erro", "Simulação. Erro no envio da tabela de ambientais (0001)");
            console.log(data);   
        }  
    });  
}

function salvaArquivosTabelas(simul){
    var Humanos   = JSON.stringify(ConteudoTabelas['humanos']);
    var Mosquitos = JSON.stringify(ConteudoTabelas['mosquitos']);
    var Simulacao = JSON.stringify(ConteudoTabelas['simulacao']);

    $.ajax({
        type : "POST",
        url : "../app/models/simulacao/SalvaParametros.php",
        data : {'humanos' : Humanos, 'mosquitos' : Mosquitos, 'simulacao' : Simulacao, 'simul' : simul},
        dataType : "json",
        async : false,
        success : function(data){
            var sucesso = true;
            if(data["Humanos"] == "erro"){
                sucesso = false;
                alerta("Erro ao Salvar", "Simulação. Não foi possivel salvar a tabela de Humanos");
            }
            if(data["Mosquitos"] == "erro"){
                sucesso = false;
                alerta("Erro ao Salvar", "Simulação. Não foi possivel salvar a tabela de Mosquitos ");
            }
            if(data["Simulacao"] == "erro"){
                sucesso = false;
                alerta("Erro ao Salvar", "Simulação. Não foi possivel salvar a tabela de Simulacao ");
            }
            if(sucesso)
                alerta("Informações Salvas", "Simulação. Os parametros foram salvos no banco");
            console.log(data);
        },
        error : function(data){
            alerta("Erro", "Simulação. Erro no envio das Tabelas");
            console.log(data);
        }  
    }); 
}

function executarSimulacaoReq(simul){
    var Humanos   = JSON.stringify(ConteudoTabelas['humanos']);
    var Mosquitos = JSON.stringify(ConteudoTabelas['mosquitos']);
    var Simulacao = JSON.stringify(ConteudoTabelas['simulacao']);
    console.log("awdawd : "+ simul);
    $.ajax({
        type : "POST",
        url : "../app/models/simulacao/Simulacao.php",
        data : {'humanos' : Humanos, 'mosquitos' : Mosquitos , 'simulacao' : Simulacao, 'simul' : simul},
        dataType : "json",
        async : true,
        success : function(data){
            var sucesso = true;
        },
        error : function(data){
            alerta("Erro", "Simulação. Erro no envio das Tabelas");
            console.log(data);
        }  
    }); 
}

function checaEmExecucao(){
    var emExec = false;
    $.ajax({
        type : "POST",
        url : "../app/models/simulacao/RetornaEmExecucao.php",
        dataType : "json",
        async : false,
        success : function(data){
            if(data == true)
                emExec = true;
        },
        error : function(data){
            alerta("Erro", "Simulação. Erro no envio das Tabelas");
            console.log(data);
        }  
    });
    return emExec;
};

function atualizaProgresso(){
    var simulacaoAt = simulacaoAtual();
    simulacaoAt = simulacaoAt[0];
}

buttonExecutar.addEventListener('click', executarSimulacao);
