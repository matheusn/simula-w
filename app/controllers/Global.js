var alertaModal = document.getElementById("alertaModal");

var tituloModalAlerta = document.getElementById("tituloModalAlerta");
var mensagemModalAlerta = document.getElementById("mensagemModalAlerta");

function alerta(titulo, mensagem){
    tituloModalAlerta.innerHTML   = titulo;
    mensagemModalAlerta.innerHTML = mensagem;

    $(alertaModal).modal('show');
}