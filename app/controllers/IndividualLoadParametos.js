function loadMapLegendasAtributos(dados){
    let legendas = [];
    for(let i = 0; i < dados.length; i++)
        legendas[dados[i]['pref']] = dados[i]['atributo'];
    return legendas;
}

function loadAtributosAgente(agente){
    var dados = [];
    $.ajax({
        type : "POST",
        url : "../app/models/individual/RetornaAtributosIndividuo.php",
        dataType: "json",
        data : {'agente' : agente},
        async : false,
        success: function(data){
            for(var i = 0; i < data.length; i++){
                dados[i] = {'visivel' : false, 'pref' : data[i]["pref"], 'atributo' : data[i]["atributo"]};
            }
        },
        error: function(){
            alert("Erro", "Individual. Não foi possivel obter os parametros");
            console.log(errorThrown);
        }
    });
    return dados;
}

function loadAtributosAgenteFiltros(agente){
    var dados = [];
    $.ajax({
        type : "POST",
        url : "../app/models/individual/RetornaAtributosIndividuosFiltros.php",
        dataType: "json",
        data : {'agente' : agente},
        async : false,
        success: function(data){ 
            for(var i = 0; i < data.length; i++){
                var param = ['--'];
                if(data[i]["valores"].length != 0)
                    param = data[i]["valores"];
                dados[i] = {'valor' : null, 'pref' : data[i]["pref"], 'atributo' : data[i]["atributo"], 'valores' : data[i]["valores"]};
            }
        },
        error: function(){
            alert("Erro","Individual. Não foi possivel obter os filtros.");
            console.log(errorThrown);
        }
    });
    return dados;
}

function trataJsonResultado(dados){
    let campos = Object.keys(dados)
    let ndados = [];
    let resp = []
    console.log(campos.length)

    for(let i = 0; i < campos.length; i++)
        ndados[campos[i]] = Object.values(dados[campos[i]])
    
    for(let i = 0; i < ndados[campos[0]].length; i++){
        resp[i] = [];
        for(let j = 0; j < campos.length; j++)
            resp[i][campos[j]] = ndados[campos[j]][i];
    }
    
    console.log(resp);
    return resp;
}
