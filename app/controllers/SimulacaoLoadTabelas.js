/*
    Matheus N Ismael 20/02/19
*/

var ConteudoTabelas     =[];
var ConteudoAmbientais;

var DivAmbiente         = document.getElementById("ambiente");
var DivHumanos          = document.getElementById("humanos");
var DivMosquitos        = document.getElementById("mosquitos");
var DivSimulacao        = document.getElementById("simulacao");
var buttonUploadArquivo = document.getElementById("buttonUploadArquivo");
var tabelas                = [];
    tabelas["humanos"]     = ["0-INI", "1-MOV", "2-CON", "3-TRA", "4-CON", "5-INS"];
    tabelas["mosquitos"]   = ["0-INI", "1-MOV", "2-CON", "3-TRA", "4-CON", "5-GER"];
    tabelas["simulacao"]   = ["0-SIM"];

//================================================================

function plotaTabela(entrada){
    for(var i = 0 ; i < tabelas[entrada].length ; i++){
        var div = "#"+entrada+"-"+tabelas[entrada][i];
        var table = new Tabulator(div,{
            data: ConteudoTabelas[entrada][tabelas[entrada][i]],
            height:"800px",
            width:"500px",
            columns:[
                {title:"Codigo", field:"codigo", headerSort:false ,width:70},
                {title:"Min", field:"min", headerSort:false, editor:"input" ,width:90, align:"center"},
                {title:"Max", field:"max", headerSort:false, editor:"input" ,width:90, align:"center"},
                {title:"Descrição", field:"descricao", headerSort:false ,width:600}, 
            ]
        });
    }
}

function plotaTabelaAmbientais(){
    var div = "#tabela-ambiente";
    var table = new Tabulator(div,{
        data: ConteudoAmbientais,
        height:"800px",
        width:"500px",
        columns:[
            {title:"Descricao", field:"key", headerSort:false ,width:280},
            {title:"Valor", field:"value", headerSort:false, editor:"input" ,width:200, align:"left"},
        ]
    });
}

function plotaTabelas(){
    plotaTabela("humanos");
    plotaTabela("mosquitos");
    plotaTabela("simulacao");
    plotaTabelaAmbientais();
}

function loadTabela(entrada, base, tabela){
    $.ajax({
        type:"POST",
        url:"../app/models/simulacao/RetornaParametrosTabelas.php",
        data : {"entrada" : entrada, "base" : base , "tabela" : "outros"},
        dataType:"json",
        async : false,
        success : function(data){
            ConteudoTabelas[entrada] = data;
            return 1;
        },
        error:function(errorThrown, data){
            alerta("Erro","Simulação. Erro ao Carregar Tabela de informações sobre "+entrada+'!');
        }
    })
}

function emptyTabelas(){
    var div = ["humanos", "mosquitos", "simulacao"];
    var tabelaAmbiente = document.getElementById("tabela-ambiente");
    for(var i = 0 ; i < div.length; i++){
        for(var j = 0 ; j < tabelas[div[i]].length ; j++){
            var id = div[i]+"-"+tabelas[div[i]][j];
            var divat = document.getElementById(id);
            $(divat).empty();
            $(tabelaAmbiente).empty();
        }
    }
}

function loadTabelaAmbientais(base){
    $.ajax({
        type:"POST",
        url:"../app/models/simulacao/RetornaParametrosTabelas.php",
        data : {"base" : base , "tabela" : "ambientais"},
        dataType:"json",
        async : false,
        success : function(data){
            ConteudoAmbientais = data;
            return 1;
        },
        error:function(errorThrown, data){
            alerta("Erro", 'Simulação. Erro ao Carregar Tabela De Dados Ambientais!');
        }
    })
}

function carregaArquivo(){
    var pasta = document.getElementById("individuoArquivoSimulacao").value;

    var file_data = $('#teste-arquivo').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        type: "POST",
        url : "../app/models/utils/CsvToData.php",
        dataType : "json",
        contentType: false,
        processData: false,
        dataType: "json",
        data: form_data,
        enctype: "multipart/form-data",
        success: function(data){
            var arquivo = data["nome"]["name"].split('.')[0];
            ConteudoTabelas[pasta][arquivo] = data["tabela"];
            plotaTabelas();
        },
        error: function(data){
            console.log(data);
        }
    });
}

function resetVisualizacao(){
    ConteudoTabelas     = [];
    ConteudoAmbientais  = null;
    hideSidebar();
    hideMenu();
    emptyTabelas();
    carregaSelectSimulacoes(inputSimulacao);
    carregaSelectSimulacoes(simulaBase);
}

buttonUploadArquivo.addEventListener('click', carregaArquivo);


//loadTabelaAmbientais("base01");
//loadTabela("simulacao", "base01");
//loadTabela("humanos", "base01");
//loadTabela("mosquitos", "base01");
//corrigir opcoes tabela (p = aleatorio; 1 - p = caminhos)//
