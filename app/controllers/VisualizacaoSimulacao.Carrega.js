var StackSimulacoes           = [];
var simul                     = false;
var cicloAtualInput           = document.getElementById("cicloAtual");
var cicloTotalInput           = document.getElementById("cicloTotal");
var individuosSelecionados    = [];
var setIndividuosSelecionados = new Set();
var individuos                = [];
var escala                    = 0.06;
var IZoom                     = false;

///////////////////////eventos\\\\\\\\\\\\\\\\\\\\\\\

function NextCiclo(){
    console.log(simul)
    if((cicloAtualInput.value +1 <= 365) && simul){
        cicloAtualInput.value++;
        atualizaCiclo();
    }
}
function PrevCiclo(){
    if((cicloAtualInput.value -1 >= 1) && simul){
        cicloAtualInput.value--;
        atualizaCiclo();
    }
}
function atualizaCiclo(){
    var ciclo = cicloAtualInput.value -1;
    if(simul){
        console.log("nc : "+ciclo);
        for(var  i = 0 ; i < StackSimulacoes.length ; i++)
            carregaPontosCiclo(StackSimulacoes[i],ciclo);
    }
}
///////////////////////filtros\\\\\\\\\\\\\\\\\\\\\\

function selecionaIndividuos(){
    var ciclo = cicloAtualInput.value -1;
    for(var  i = 0 ; i < StackSimulacoes.length ; i++)
        carregaPontosCiclo(StackSimulacoes[i], ciclo);
}

function findNomeAgente(cod){
    for(var  i = 0 ; i < individuos.length ; i++){
        if(individuos[i].cod == cod)
            return individuos[i].nome;
    }
};

///////////////////////carregamento da simulação\\\\\\\\\\\\\\\\\\\\\\\

function createSt(src) {
    return new ol.style.Style({
        image: new ol.style.Icon(({
            src: src,
            scale: escala
        }))
      });
}

function retIndividuo(cod){
    for(var i = 0 ; i < individuos.length ; i++)
        if(individuos[i].cod == cod)
            return individuos[i];
}

function retorna_individuos(){
    var individuosAT = new Array();

    for(var i=0 ; i < individuosSelecionados.length ; i++){

        var found = individuosAT.find(function(element) {
            return element == individuosSelecionados[i];
        });

        if(individuosSelecionados[i].cod == 10000){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(14001));
            individuosAT.push(retIndividuo(14010));
            individuosAT.push(retIndividuo(14011));
            individuosAT.push(retIndividuo(14020));
            individuosAT.push(retIndividuo(14021));
            individuosAT.push(retIndividuo(14030));
            individuosAT.push(retIndividuo(14031));
        }
        else if(individuosSelecionados[i].cod == 20000){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(24001));
            individuosAT.push(retIndividuo(24010));
            individuosAT.push(retIndividuo(24011));
            individuosAT.push(retIndividuo(24020));
            individuosAT.push(retIndividuo(24021));
            individuosAT.push(retIndividuo(24030));
            individuosAT.push(retIndividuo(24031));
        }
        else if(individuosSelecionados[i].cod == 30000){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(34001));
            individuosAT.push(retIndividuo(34010));
            individuosAT.push(retIndividuo(34011));
            individuosAT.push(retIndividuo(34020));
            individuosAT.push(retIndividuo(34021));
            individuosAT.push(retIndividuo(34030));
            individuosAT.push(retIndividuo(34031));
        }
        else if(individuosSelecionados[i].cod == 40000){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(44001));
            individuosAT.push(retIndividuo(44010));
            individuosAT.push(retIndividuo(44011));
            individuosAT.push(retIndividuo(44020));
            individuosAT.push(retIndividuo(44021));
            individuosAT.push(retIndividuo(44030));
            individuosAT.push(retIndividuo(44031));
        }
        else if(individuosSelecionados[i].cod == 50000){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(54001));
            individuosAT.push(retIndividuo(54010));
            individuosAT.push(retIndividuo(54011));
            individuosAT.push(retIndividuo(54020));
            individuosAT.push(retIndividuo(54021));
            individuosAT.push(retIndividuo(54030));
            individuosAT.push(retIndividuo(54031));
        }
        else if(individuosSelecionados[i].cod == 4010){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(14010));
            individuosAT.push(retIndividuo(14011));
            individuosAT.push(retIndividuo(24010));
            individuosAT.push(retIndividuo(24011));
            individuosAT.push(retIndividuo(34010));
            individuosAT.push(retIndividuo(34011));
            individuosAT.push(retIndividuo(44010));
            individuosAT.push(retIndividuo(44011));
            individuosAT.push(retIndividuo(54010));
            individuosAT.push(retIndividuo(54011));
            individuosAT.push(retIndividuo(4011));
        }
        else if(individuosSelecionados[i].cod == 4020){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(14020));
            individuosAT.push(retIndividuo(14021));
            individuosAT.push(retIndividuo(24020));
            individuosAT.push(retIndividuo(24021));
            individuosAT.push(retIndividuo(34020));
            individuosAT.push(retIndividuo(34021));
            individuosAT.push(retIndividuo(44020));
            individuosAT.push(retIndividuo(44021));
            individuosAT.push(retIndividuo(54020));
            individuosAT.push(retIndividuo(54021));
            individuosAT.push(retIndividuo(4021));
        }
        else if(individuosSelecionados[i].cod == 4030){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(14030));
            individuosAT.push(retIndividuo(14031));
            individuosAT.push(retIndividuo(24030));
            individuosAT.push(retIndividuo(24031));
            individuosAT.push(retIndividuo(34030));
            individuosAT.push(retIndividuo(34031));
            individuosAT.push(retIndividuo(44030));
            individuosAT.push(retIndividuo(44031));
            individuosAT.push(retIndividuo(54030));
            individuosAT.push(retIndividuo(54031));
            individuosAT.push(retIndividuo(4031));
        }
        else if(individuosSelecionados[i].cod == 4001){
            individuosAT.push(individuosSelecionados[i]);
            individuosAT.push(retIndividuo(14001));
            individuosAT.push(retIndividuo(14011));
            individuosAT.push(retIndividuo(14021));
            individuosAT.push(retIndividuo(14031));

            individuosAT.push(retIndividuo(24001));
            individuosAT.push(retIndividuo(24011));
            individuosAT.push(retIndividuo(24021));
            individuosAT.push(retIndividuo(24031));

            individuosAT.push(retIndividuo(34001));
            individuosAT.push(retIndividuo(34011));
            individuosAT.push(retIndividuo(34021));
            individuosAT.push(retIndividuo(34031));

            individuosAT.push(retIndividuo(44001));
            individuosAT.push(retIndividuo(44011));
            individuosAT.push(retIndividuo(44021));
            individuosAT.push(retIndividuo(44031));

            individuosAT.push(retIndividuo(54001));
            individuosAT.push(retIndividuo(54011));
            individuosAT.push(retIndividuo(54021));
            individuosAT.push(retIndividuo(54031));

            individuosAT.push(retIndividuo(4011));
            individuosAT.push(retIndividuo(4021));
            individuosAT.push(retIndividuo(4031));
        }
        else if(!found)
            individuosAT.push(individuosSelecionados[i]);
    }
    return individuosAT;
}

function retornaIcone(icone){

    if(!individuosSelecionados.find(function(element){return element == retIndividuo(icone);})){
        return icone;
    }
    else{

        var ic10000 = [14001,14010,14011,14020,14021,14030,14031];
        var ic20000 = [24001,24010,24011,24020,24021,24030,24031];
        var ic30000 = [34001,34010,34011,34020,34021,34030,34031];
        var ic40000 = [44001,44010,44011,44020,44021,44030,44031];
        var ic50000 = [54001,54010,54011,54020,54021,54030,54031];

        var ic4010  = [14010,14011,24010,24011,34010,34011,44010,44011,54010,54011];
        var ic4020  = [14020,14021,24020,24021,34020,34021,44020,44021,54020,54021];
        var ic4030  = [14030,14031,24030,24031,34030,34031,44030,44031,54030,54031];

        var ic4001  = [14001,14011,14021,14031,24001,24011,24021,24031,34001,34011,34021,34031,44001,44011,44021,44031,54001,54011,54021,54031,4011,4021,4031];

        if(ic10000.find(function(element){return element == icone;}))
            return 10000;
        if(ic20000.find(function(element){return element == icone;}))
            return 20000;
        if(ic30000.find(function(element){return element == icone;}))
            return 30000;
        if(ic40000.find(function(element){return element == icone;}))
            return 40000;
        if(ic50000.find(function(element){return element == icone;}))
            return 50000;
        if(ic4010.find(function(element){return element == icone;}))
            return 4010;
        if(ic4020.find(function(element){return element == icone;}))
            return 4020;
        if(ic4030.find(function(element){return element == icone;}))
            return 4030;
        if(ic4001.find(function(element){return element == icone;}))
            return 4001;
    }
}

function selectSimulacao(){
    var arquivo = document.getElementById("simulacoes").value;

    var found = StackSimulacoes.find(function(element) {
        return element == arquivo;
      });

    if(!found){
        cicloAtualInput.removeAttribute('readonly');
        cicloAtualInput.value = 1;
        simul = true;

        atualizaTotalCiclos(arquivo);

        carregaPontosCiclo(arquivo,0);
    }
    else{
        alert("O arquivo já esta presente na visualização");
    }   
}

function carregaPontosCiclo(arquivo,ciclo){
    console.log("lalalalalala");
    var individuosLoad;
    var found = false;

    toggleLoaderMap(true);

    for(var  i = 0 ; i < StackSimulacoes.length ; i++){
        if(arquivo == StackSimulacoes[i])
            found = true;
    }
    if(found == false)
        individuosLoad = [];
    else
        individuosLoad = retorna_individuos();

    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/mapas/simulacoes/CarregaPontosSimulacao.php",
        dataType: "json",
        data: { 'arquivo': arquivo , 'ciclo': ciclo , 'individuos':individuosLoad},
        success: function(data) {               
            var existe = false;
            var indice;
            for (var i = 0; i < visivel.length; i++) {
                if (visivel[i]['nome'] == arquivo) {
                    existe = true;
                    indice = visivel[i][0];
                }
            }
            console.log(data);
            var vectorSource = new ol.source.Vector();
            for (var i = 0; i < data.length; i++) {
                ponto = data[i].coord.split(' ');
                var iconFeature = new ol.Feature(new ol.geom.Point([ponto[0], ponto[1]]));
                center = ponto;

                iconFeature.set('style', createSt('../../imagens/icones/'+  retornaIcone(data[i].icone)+'.png'));
                
                vectorSource.addFeature(iconFeature);
                if(!setIndividuosSelecionados.has(data[i].icone)){
                    setIndividuosSelecionados.add(data[i].icone);

                    var ind = individuosSelecionados.length;
                    individuosSelecionados[ind] = new Object();
                    individuosSelecionados[ind].cod  = retornaIcone(data[i].icone);
                    individuosSelecionados[ind].nome = findNomeAgente(data[i].icone);
                }
            }
            
            var vectorLayerPontos = new ol.layer.Vector({ source: vectorSource, style :function(feature){return feature.get('style'); }});

            vectorLayerPontos.set('name', arquivo);

            if(existe)//se a simulação já está presente na view apenas a atualiza
            {
                atualizaSimulacao(vectorLayerPontos, indice);
                
            }
            else{ //adiciona a simulação a view
                insereSimulacao(vectorLayerPontos,arquivo,[ponto[0] , ponto[1]]);
                StackSimulacoes.push(arquivo);
                view.centerOn([ponto[0] , ponto[1]], map.getSize(), [500, 200]);
            }
            loadSelectIndividuosSelecionados();
            toggleLoaderMap(false);
        },
        error: function(errorThrown, data) {
            alert('Erro ao Carregar!');
            console.log(errorThrown);
            console.log(data);
        }
    });
}

function atualizaTotalCiclos(arquivo){
    $.ajax({
        type: "POST",
        url: "../app/models/visualizacao/mapas/simulacoes/RetornaNumeroCiclos.php",
        dataType: "json",
        data: { 'arquivo': arquivo},
        success: function(data) {
            cicloTotalInput.value =  data.count - 2 ;
        },
        error: function(errorThrown, data) {
            alert('Erro ao Carregar!');
            console.log(errorThrown);
            console.log(data);
        }
    });
}

function insereSimulacao(vectorLayerPontos,arquivo,centro){
    map.addLayer(vectorLayerPontos);
    camadas.push(vectorLayerPontos);
    /*
    visivel.push(0);
    visivel[visivel.length - 1] = new Array();
    visivel[visivel.length - 1].push(camadas.length - 1);
    visivel[visivel.length - 1].push(true);
    visivel[visivel.length - 1].push(arquivo);
    visivel[visivel.length - 1].push(true);
    visivel[visivel.length - 1].push("simulacao");
    visivel[visivel.length - 1].push(centro);
    */
    let id = idLayers++;
    let tipo = "simulacao";

    visivel.push({
        id : id,
        indexCamadas : camadas.length - 1,
        visivel : true,
        nome : arquivo,
        tipo : tipo,
        centro : centro,
    });

    document.getElementById("filtro").style.display = "";
    AdicionaIconeMapa(id, arquivo, tipo);
}

function atualizaSimulacao(vectorLayerPontos,indice){
    if(camadas[indice] == vectorLayerPontos)
        Console.log("teste");
    camadas[indice] = vectorLayerPontos;
    atualizaMapa();
}

function RemoveSimulacao(nomesimulacao){
    for(var i = 0 ; i < StackSimulacoes.length ; i++){
        if(StackSimulacoes[i] == nomesimulacao)
            StackSimulacoes.splice(i,1);
    }

    if(StackSimulacoes.length == 0){
        simul = false;
        cicloAtualInput.value = 0;
        cicloTotalInput.value = 0;
        cicloAtualInput.setAttribute('readonly',true);
        document.getElementById("filtro").style.display = "none";
    }
}








