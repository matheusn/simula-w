/*
    Matheus N Ismael 20/02/19

*/
var inputSimulacao = document.getElementById("simulaSelect");
var simulaBase    = document.getElementById("simulaSelectNova");
var Simulacoes = [];
var titulo = document.getElementById("titulo");
var statusSim = document.getElementById("statusSim");
var ambiente = document.getElementById("ambiente");
var doenca = document.getElementById("doenca");
var duracao = document.getElementById("duracao");
var infosSimulacao = document.getElementById("infosSimulacao");

//================================================================

function simulacaoAtual(){
   for(var  i = 0 ; i < Simulacoes.length ;i++)
       if(Simulacoes[i]['nome'] == inputSimulacao.value)
           return Simulacoes[i];
    return null;   
}

function carregaSelectSimulacoes(select){
    $(select).empty();
    var option = document.createElement("option");
    option.innerHTML = "--";
    option.selected = true;
    select.appendChild(option);

    $.ajax({
        type : "POST",
        url : "../app/models/simulacao/RetornaInfosSimulacoes.php",
        dataType: "json",
        success: function(data){
            Simulacoes = data;
            for(var  i = 0 ; i < Simulacoes.length ; i++){
                var option = document.createElement("option");
                option.innerHTML = Simulacoes[i]['nome'];
                select.appendChild(option);
            }
        },
        error: function(){
            alerta("Erro","Simulação. Não foi possivel obter a Listagem de simulações!");
            console.log(errorThrown);
         }
    });
} 


function atualizaMenu(){
    infosSimulacao.style.display = 'block';
    var simulacaoAT = simulacaoAtual();
    titulo.innerHTML = simulacaoAT['nome'].toUpperCase();
    statusSim.innerHTML = simulacaoAT['status'].toUpperCase();
    ambiente.innerHTML = simulacaoAT['ambiente'].toUpperCase();
    doenca.innerHTML = simulacaoAT['doenca'].toUpperCase();
    duracao.innerHTML = simulacaoAT['duracaoano'];
}
function hideMenu(){
    infosSimulacao.style.display = 'none';
    titulo.innerHTML    = "";
    statusSim.innerHTML = "";
    ambiente.innerHTML  = "";
    doenca.innerHTML    = "";
    duracao.innerHTML   = "";
}

inputSimulacao.addEventListener('change',atualizaMenu);