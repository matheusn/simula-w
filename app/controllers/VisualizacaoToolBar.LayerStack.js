//cria icone da layer na pilha de layers
function AdicionaIconeMapa(id, nomemapa, tipo) {
    let menu     = document.getElementById("listaMapas");
    let icone    = document.createElement('li');
    let checkbox = document.createElement('input');
    let nome     = document.createElement('span');

    console.log("id : "+ id);


    checkbox.type = "checkbox";
    checkbox.id   = "check"+id;
    $(checkbox).prop('checked', true);
    $(checkbox).addClass("checkboxitemmapa");

    $(icone).addClass("itemMapa");
    icone.id = id + "li";

    $(nome).addClass("nomeLayer");
    icone.title    = nomemapa;
    nome.innerHTML = nomemapa;

    icone.appendChild(nome);

    //menu da layer
    let menuDrop = document.createElement('div');
        $(menuDrop).addClass("dropdown");
            menuDrop.innerHTML = '<button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';   
        
            let dropDownMenu = document.createElement('div');
                $(dropDownMenu).addClass("dropdown-menu");


            //mudar estilo da layer
                let estilo = document.createElement('span');
                    estilo.innerHTML = "Estilo";
                    $(estilo).addClass("dropdown-item");
                    //evento
                        estilo.addEventListener('click', function() {
                            for (let i = 0; i < visivel.length; i++) {
                                if (visivel[i]['id'] == id) {
                                    if (visivel[i]['tipo'] == "simulacao"){
                                        let sizeIcone = document.getElementById("escalaIcone").value = escala * 1000;
                                        $('#styleEditaSimulacao').modal('show');
                                    }
                                    else if(visivel[i]['tipo'] == "mapa"){
                                        $('#styleEditaMapas').modal('show');
                                        let pVetCamadas = visivel[i]['indexCamadas'];
                                        let FillCoratual = camadas[pVetCamadas].getStyle().fill_.color_;
                                        let StrokeCor    = camadas[pVetCamadas].getStyle().stroke_.color_;
                                        let StrokeSize   = camadas[pVetCamadas].getStyle().stroke_.width_;
                                        carregaEstilo(FillCoratual, StrokeCor , StrokeSize , i);   
                                    }
                                    else if(visivel[i]['tipo'] == "pontos"){
                                        //por fazer
                                    }
                                }
                            }
                        });
                dropDownMenu.appendChild(estilo);

            //remover a layer
                let remove = document.createElement('a');
                    remove.innerHTML = "Remover";
                    $(remove).addClass("dropdown-item");
                    //evento
                        remove.addEventListener('click', function() {
                            for (let i = 0; i < visivel.length; i++) {
                                if (visivel[i]['id'] == id) {
                                    camadas.splice(visivel[i]['indexCamadas'], 1);
                                    visivel.splice(i, 1);
                                    for (let j = 0; j < visivel.length; j++)
                                        visivel[j]['indexCamadas'] = j;
                                }
                            }
                            document.getElementById(id + "li").remove();
                            atualizaMapa();
                            if(tipo == "simulacao")
                                RemoveSimulacao(nomemapa);
                        });
                dropDownMenu.appendChild(remove);
                
            //trazer para frente na visualização
                let subir = document.createElement('a');
                    subir.innerHTML = "Subir Layer";
                    $(subir).addClass("dropdown-item");
                    //evento
                        subir.addEventListener('click', function(){
                            for (let i = 0; i < visivel.length; i++) {
                                if (visivel[i]['id'] == id) {
                                   if(i < visivel.length -1 ){
                                        let temp = visivel[i];
                                        visivel[i] = visivel[i+1];
                                        visivel[i + 1] = temp;
                                        break;
                                   }
                                }
                            }
                            $("#listaMapas").empty();
                            recarregaIcones();
                            atualizaMapa();
                        });
                dropDownMenu.appendChild(subir);

            //Aproximar para camada
                let aproximar = document.createElement('a');
                    aproximar.innerHTML = "Aproximar";
                    $(aproximar).addClass("dropdown-item");
                    //evento
                        aproximar.addEventListener('click', function(){
                            for (let i = 0; i < visivel.length; i++) {
                                if (visivel[i]['id'] == id) {
                                    view.centerOn(visivel[i]['centro'], map.getSize(), [500, 200]);
                                   break;
                                }
                            }
                        });
                dropDownMenu.appendChild(aproximar);

    menuDrop.appendChild(dropDownMenu);
    //\menu da layer

    checkbox.addEventListener('click', function() {
        let check = document.getElementById("check"+id);
        if (!check.checked) {
            RemoverMapaDaView(id);
            atualizaMapa();
            console.log("invisivel" + id)
        } else {
            for (let i = 0; i < visivel.length; i++) 
                if (visivel[i]['id'] == id)
                    visivel[i]['visivel'] = true;
            atualizaMapa();
            console.log("visivel" + id)
        }
    });
    icone.appendChild(checkbox);
    icone.appendChild(menuDrop);
    firstChild = menu.firstChild;
    menu.insertBefore(icone, firstChild);
}

///loader 

function toggleLoaderMap(trigger){
    if(trigger)
        document.getElementById("loader").style.display = "block";
    else
        document.getElementById("loader").style.display = "none";
}
