#include <iostream>
#include <limits>
#include <set>
#include <map>
#include <cmath>

using namespace std;

typedef struct SPonto {
	int x, y;
} Ponto;

typedef struct SRota {
	int pRua; // Identificador da primeira rua
	Ponto origem; // Centroide do lote de origem
	Ponto destino; // Centroide do lote de destino
	int qOrig, lOrig; // Identificadores de quadra e lote de origem
	int qDest, lDest; // Identificadores de quadra e lote de destino
	int tamSaida, *saida;
} Rota;

class AStar {
private:
	Rota *rotas;
	int *verts;
	int nVerts, nRotas;
	set<int> openSet;
	set<int> closedSet;
	map<int, double> gScore;
	map<int, double> fScore;
	map<int, int> prev;
	map<int, map<int, Ponto> > esquinas;
	map<int, map<int, set<int> > > ruasDestino;
	void executar(int id);
	double dist(Ponto a, Ponto b);
	int getCurrent();
	Ponto getCentrEsquina(int current, Ponto destino);
public:
	~AStar();
	void setVerts(int nVerts, int *idLotes);
	void setRotas(int nRotas, int *pRuas);
	void setOrigem(int *x, int *y);
	void setDestino(int *x, int *y);
	void setQuadrasLotes(int *qa, int *la, int *qb, int *lb);
	void setEsquinas(int n, int *a, int *b, int *x, int *y);
	void setRuasDestino(int n, int *q, int *l, int *r);
	void proc();
	void saidaIndexRotas(int *indexRotas);
	void saidaVetorRotas(int *vetorRotas);
};

AStar::~AStar() {
	for (int i = 0; i < nRotas; ++i) delete[] rotas[i].saida;
	delete[] verts;
	delete[] rotas;
}

void AStar::setVerts(int nVerts, int *idLotes) {
	this->nVerts = nVerts;
	verts = new int[nVerts];
	for (int i = 0; i < nVerts; ++i) {
		verts[i] = idLotes[i];
	}
}

void AStar::setRotas(int nRotas, int *pRuas) {
	this->nRotas = nRotas;
	rotas = new Rota[nRotas];
	for (int i = 0; i < nRotas; ++i) {
		rotas[i].pRua = pRuas[i];
	}
}

void AStar::setOrigem(int *x, int *y) {
	for (int i = 0; i < nRotas; ++i) {
		rotas[i].origem = Ponto();
		rotas[i].origem.x = x[i];
		rotas[i].origem.y = y[i];
	}
}

void AStar::setDestino(int *x, int *y) {
	for (int i = 0; i < nRotas; ++i) {
		rotas[i].destino = Ponto();
		rotas[i].destino.x = x[i];
		rotas[i].destino.y = y[i];
	}
}

void AStar::setQuadrasLotes(int *qa, int *la, int *qb, int *lb) {
	for (int i = 0; i < nRotas; ++i) {
		rotas[i].qOrig = qa[i];
		rotas[i].lOrig = la[i];
		rotas[i].qDest = qb[i];
		rotas[i].lDest = lb[i];
	}
}

void AStar::setEsquinas(int n, int *a, int *b, int *x, int *y) {
	esquinas = map<int, map<int, Ponto> >();
	for (int i = 0; i < n; ++i) {
		esquinas[a[i]][b[i]] = Ponto();
		esquinas[a[i]][b[i]].x = x[i];
		esquinas[a[i]][b[i]].y = y[i];
	}
}

void AStar::setRuasDestino(int n, int *q, int *l, int *r) {
	ruasDestino = map<int, map<int, set<int> > >();
	for (int i = 0; i < n; ++i) {
		int quadra = q[i];
		int lote = l[i];
		int rua = r[i];
		ruasDestino[quadra][lote].insert(rua);
	}
}

void AStar::proc() {
	cout << "nRotas: " << nRotas << endl;
	for (int i = 0; i < nRotas; ++i) {
		executar(i);
	}
}

void AStar::saidaIndexRotas(int *indexRotas) {
	int sum = 0;
	indexRotas[0] = sum;
	for (int i = 0; i < nRotas; ++i) {
		sum += rotas[i].tamSaida;
		indexRotas[i + 1] = sum;
	}
}

void AStar::saidaVetorRotas(int *vetorRotas) {
	int pos = 0;
	for (int i = 0; i < nRotas; ++i) {
		Rota rota = rotas[i];
		for (int j = 0; j < rota.tamSaida; ++j) {
			vetorRotas[pos++] = rota.saida[j];
		}
	}
}

void AStar::executar(int id) {

	Rota rota = rotas[id];

	// Inicializa os mapas e conjuntos
	openSet = set<int>();
	closedSet = set<int>();
	gScore = map<int, double>();
	fScore = map<int, double>();
	prev = map<int, int>();

	for (int i = 0; i < nVerts; ++i) {
		int vert = verts[i];
		gScore[vert] = numeric_limits<double>::max();
		fScore[vert] = numeric_limits<double>::max();
		prev[vert] = -1;
	}

	// Inicializa o primeiro vértice
	gScore[rota.pRua] = 0;
	fScore[rota.pRua] = dist(rota.origem, rota.destino);
	openSet.insert(rota.pRua);

	while (!openSet.empty()) { // Enquanto openSet não estiver vazio

		int current = getCurrent(); // Determina o nó atual
		set<int> goal = ruasDestino[rota.qDest][rota.lDest];

		// Checa se o nó atual faz fronteira com o destino
		if (goal.find(current) != goal.end()) {

			// Contabiliza o total de vértices percorridos
			int tamSaida = 1, vert = current;
			while (prev[vert] != -1) {
				vert = prev[vert];
				++tamSaida;
			}
			tamSaida += 4; // Reserva espaço para ids de lote e quadra
			int *saida = new int[tamSaida];

			// Armazena os ids de lote e quadra
			saida[0] = rota.lOrig;
			saida[1] = rota.qOrig;
			saida[2] = rota.lDest;
			saida[3] = rota.qDest;

			// Armazena os vértices percorridos
			int i = tamSaida - 1;
			vert = current;
			saida[i] = vert;
			while (prev[vert] != -1) {
				vert = prev[vert];
				saida[--i] = vert;
			}

			rotas[id].tamSaida = tamSaida;
			rotas[id].saida = saida;
			return;
		}
			
		// Move o nó atual para o closedSet
		openSet.erase(current);
		closedSet.insert(current);

		// Determina a esquina mais próxima ao destino da rota
		Ponto centr = getCentrEsquina(current, rota.destino);

		// Acesso ao map de vizinhos
		map<int, Ponto> neighbors = esquinas[current];
		map<int, Ponto>::iterator it;

		// Para cada vizinho de current
		for (it = neighbors.begin(); it != neighbors.end(); ++it) {

			int neighbor = it->first;

			// Ignora os vizinhos pertencentes ao closedSet
			if (closedSet.find(neighbor) != closedSet.end()) continue;

			// Cálculo do gScore auxiliar
			double gScoreAux = gScore[current] + dist(centr, it->second);

			if (openSet.find(neighbor) == openSet.end()) {
				// Adiciona o vizinho ao openSet caso não esteja
				openSet.insert(neighbor);
			} else if (gScoreAux >= gScore[neighbor]) {
				// Ignora caso já exista um caminho melhor
				continue;
			}

			// Atualiza as informações do vizinho
			prev[neighbor] = current;
			gScore[neighbor] = gScoreAux;

			// Estima a distância até o destino passando pelo vizinho
			double d = dist(it->second, rota.destino);
			fScore[neighbor] = gScoreAux + d;
			fScore[rota.pRua] = d;
		}
	}

	cout << "Falha na execução do algoritmo A*" << endl;
}

double AStar::dist(Ponto a, Ponto b) {
	int dx = b.x - a.x;
	int dy = b.y - a.y;
	return sqrt((double) (dx * dx + dy * dy));
}

int AStar::getCurrent() {
	// Retorna o elemento de openSet com o menor fScore
	int current;
	double minScore = numeric_limits<double>::max();
	set<int>::iterator it;
	for (it = openSet.begin(); it != openSet.end(); ++it) {
		int vert = *it;
		double score = fScore[vert];
		if (score < minScore) {
			current = vert;
			minScore = score;
		}
	}
	return current;
}

Ponto AStar::getCentrEsquina(int current, Ponto destino) {

	map<int, Ponto> neighbors = esquinas[current];
	map<int, Ponto>::iterator it;

	Ponto centr;
	double distMin = numeric_limits<double>::max();

	for (it = neighbors.begin(); it != neighbors.end(); ++it) {
		Ponto p = Ponto();
		p.x = it->second.x;
		p.y = it->second.y;
		double distP = dist(p, destino);
		if (distP < distMin) {
			centr = p;
			distMin = distP;
		}
	}

	return centr;
}

extern "C" {
	
	AStar *AStar_new() {
		return new AStar();
	}

	void AStar_delete(AStar *aStar) {
		delete aStar;
	}

	void AStar_setVerts(AStar *aStar, int nVerts, void *idLotes) {
		aStar->setVerts(nVerts, (int *) idLotes);
	}

	void AStar_setRotas(AStar *aStar, int nRotas, void *pRuas) {
		aStar->setRotas(nRotas, (int *) pRuas);
	}

	void AStar_setOrigem(AStar *aStar, void *x, void *y) {
		aStar->setOrigem((int *) x, (int *) y);
	}

	void AStar_setDestino(AStar *aStar, void *x, void *y) {
		aStar->setDestino((int *) x, (int *) y);
	}

	void AStar_setQuadrasLotes(AStar *aStar, void *qa, void *la, void *qb,
							   void *lb) {
		aStar->setQuadrasLotes((int *) qa, (int *) la, (int *) qb, (int *) lb);
	}

	void AStar_setEsquinas(AStar *aStar, int n, void *a, void *b, void *x,
						   void *y) {
		aStar->setEsquinas(n, (int *) a, (int *) b, (int *) x, (int *) y);
	}

	void AStar_setRuasDestino(AStar *aStar, int n, void *q, void *l, void *r) {
		aStar->setRuasDestino(n, (int *) q, (int *) l, (int *) r);
	}

	void AStar_proc(AStar *aStar) {
		aStar->proc();
	}

	void AStar_saidaIndexRotas(AStar *aStar, void *indexRotas) {
		aStar->saidaIndexRotas((int *) indexRotas);
	}

	void AStar_saidaVetorRotas(AStar *aStar, void *vetorRotas) {
		aStar->saidaVetorRotas((int *) vetorRotas);
	}
}
