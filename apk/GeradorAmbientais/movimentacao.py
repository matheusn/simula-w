#!/usr/bin/env python3

from a_star.wrapper import AStar
import pandas as pd
import numpy as np
import sys
import banco
import random

def print_arr_np(arr, sep=';'):
	"""Imprime uma string formatada com os elementos de um vetor."""
	if type(arr) is not np.ndarray:
		arr = np.asarray(arr)
	arr.tofile(sys.stdout, sep=sep)
	print()  # Nova linha

class Movimentacao:

	__slots__ = ['params', 'lotes', 'vertices', 'locais', 'tipos_trajetos',
				 'trajetos', 'periodos', 'rotas', 'arestas', 'ambiente',
				 'centroides_lotes', 'ruas_destino', 'index_rotas', 'simul',
				 'vetor_rotas']

	def __init__(self, params, ambiente, simul):
		print('Processando movimentação...')
		self.params = params
		self.ambiente = ambiente
		self.simul = simul
		self.ler_dados()
		self.distribuir_locais()
		self.proc_trajetos()
		self.proc_periodos()
		self.proc_rotas()
		self.proc_caminhos()

	def ler_dados(self):
		"""Acessa os dados de movimentação do banco de dados."""
		ambiente = self.params['Ambiente']
		self.lotes, self.vertices = banco.ler_poligonos(ambiente)
		self.tipos_trajetos = banco.ler_tipos_trajetos()
		self.arestas = banco.ler_arestas(ambiente)
		self.centroides_lotes = banco.ler_centroides_lotes(ambiente)

	def distribuir_locais(self):
		self.locais = {}
		lotes = self.lotes.copy()
		for local in ['Casa', 'Trabalho', 'Lazer', 'Estudo']:
			qtde = int(self.params['Lotes{}'.format(local)])
			lotes = lotes.sample(frac=1).reset_index(drop=True)
			self.locais[local] = lotes[:qtde]
			lotes = lotes[qtde:]

	def proc_trajetos(self):
		df = pd.DataFrame()
		for fe in self.tipos_trajetos.keys():  # Para cada faixa etária
			for _, casa in self.locais['Casa'].iterrows():
				row = {}
				row['fe'] = fe
				row['lote'] = casa.lote
				row['quadra'] = casa.quadra
				df = df.append(row, ignore_index=True)
		# Atribui valores para as colunas id, locais e periodos
		get_id = lambda fe: random.randrange(len(self.tipos_trajetos[fe]))
		get_loc = lambda fe, id_: self.tipos_trajetos[fe].iloc[id_].locais
		get_per = lambda fe, id_: self.tipos_trajetos[fe].iloc[id_].periodos
		df['id'] = np.vectorize(get_id)(df.fe)
		df['locais'] = np.vectorize(get_loc)(df.fe, df.id)
		df['periodos'] = np.vectorize(get_per)(df.fe, df.id)
		df.drop(columns=['id'], inplace=True)
		self.trajetos = df

	def proc_periodos(self):
		self.periodos = pd.DataFrame()
		self.periodos['periodos'] = self.trajetos.periodos
		# Adiciona a coluna de quantidade de períodos por trajeto
		contar = lambda p: len(p.split(','))
		self.periodos['qtde'] = np.vectorize(contar)(self.periodos.periodos)
		# Multiplica os períodos pela duração do dia
		duracao_dia = int(self.params['DuraçãoDia'])
		def conv(p):
			periodos = list(map(float, p.split(',')))
			periodos = [i * duracao_dia for i in periodos]
			periodos = list(map(int, periodos))
			periodos = list(map(str, periodos))
			return ';'.join(periodos)
		self.periodos.periodos = np.vectorize(conv)(self.periodos.periodos)

	def proc_rotas(self):
		df = self.trajetos.copy()
		sortear_locais = np.vectorize(self.sortear_locais)
		df.locais = sortear_locais(df.locais, df.lote, df.quadra)
		# Organiza origens e destinos das rotas
		get_l1 = lambda loc: ','.join(loc.split(',')[:-2:2])
		get_q1 = lambda loc: ','.join(loc.split(',')[1:-2:2])
		get_l2 = lambda loc: ','.join(loc.split(',')[2::2])
		get_q2 = lambda loc: ','.join(loc.split(',')[3::2])
		df['l1'] = np.vectorize(get_l1)(df.locais)
		df['q1'] = np.vectorize(get_q1)(df.locais)
		df['l2'] = np.vectorize(get_l2)(df.locais)
		df['q2'] = np.vectorize(get_q2)(df.locais)
		# Cria um DataFrame de rotas
		join_lists = lambda val: ','.join(list(val)).split(',')
		self.rotas = pd.DataFrame()
		self.rotas['l1'] = join_lists(df.l1.values)
		self.rotas['q1'] = join_lists(df.q1.values)
		self.rotas['l2'] = join_lists(df.l2.values)
		self.rotas['q2'] = join_lists(df.q2.values)

	def sortear_locais(self, locais, lote, quadra):
		"""Sorteia os locais que farão parte de um trajeto."""
		locais = locais.split(',')
		ret = []
		for local in locais:
			if local == 'Casa':  # Adiciona o lote de casa
				ret.append(lote)
				ret.append(quadra)
			else:  # Sorteia de acordo com o local
				row = self.locais[local].sample(n=1).iloc[0]
				ret.append(row.lote)
				ret.append(row.quadra)
		return ','.join(ret)  # Retorna uma string

	def proc_caminhos(self):
		# Adequação dos dados
		self.proc_primeiras_ruas()
		self.proc_ruas_de_destino()
		self.proc_centroides_lotes()
		self.proc_id_quadras_e_lotes()
		# Transfere os dados necessários para a biblioteca externa
		a_star = AStar()
		a_star.set_verts(self.vertices)
		a_star.set_rotas(self.rotas)
		a_star.set_esquinas(self.ambiente.centroides)
		a_star.set_ruas_destino(self.ruas_destino)
		# Executa o algoritmo A*
		import time
		ref_time = time.time()
		a_star.proc()
		print('Execução A*:', time.time() - ref_time)
		# Armazena as saídas indexRotas e vetorRotas
		self.index_rotas, self.vetor_rotas = a_star.output(len(self.rotas))

	def proc_primeiras_ruas(self):
		"""Adiciona a coluna 'p_rua' ao DataFrame de rotas."""
		fronteiras = self.ambiente.fronteiras
		df = fronteiras.groupby(['quadra', 'lote', 'rua']).size()
		df = pd.DataFrame(df, columns=['pts'])
		df.sort_values(by=['pts'], ascending=False, inplace=True)
		df = df.groupby(['quadra', 'lote']).head(1)
		df.reset_index(inplace=True)
		# Cria as colunas l1, q1 e p_rua
		df['l1'] = df.lote
		df['q1'] = df.quadra
		df['p_rua'] = df.rua
		# Remove as colunas quadra, lote, pts e rua
		df.drop(columns=['quadra', 'lote', 'pts', 'rua'], inplace=True)
		self.rotas = self.rotas.merge(df, on=['q1', 'l1'], how='left')

	def proc_ruas_de_destino(self):
		"""Retorna as arestas que ligam ruas aos lotes de destino."""
		df1 = self.arestas
		df2 = self.ambiente.viz
		# Filtra as entradas com origem em ruas e destino em lotes
		df1 = df1.query("q1 == '0000' & q2 != '0000'").copy()
		df2 = df2.query("quadra == '0000' & q2 != '0000'").copy()
		# Organiza as vizinhanças
		df2['q1'] = df2.quadra
		df2['l1'] = df2.lote
		df2.drop(columns=['quadra', 'lote'], inplace=True)
		df2 = df2.groupby(['l1', 'q1', 'l2', 'q2']).size()
		df2 = pd.DataFrame(df2, columns=['pts'])
		df2.reset_index(inplace=True)
		# Junta os DataFrames
		df = df1.merge(df2, on=['l1', 'q1', 'l2', 'q2'], how='inner')
		df.drop(columns=['pts'], inplace=True)
		df.sort_values(by=['q2', 'l2', 'q1', 'l1'], inplace=True)
		df.reset_index(drop=True, inplace=True)
		self.ruas_destino = df

	def proc_centroides_lotes(self):
		"""Adiciona as coordenadas dos centroides ao DataFrame de rotas."""
		df1 = self.centroides_lotes.copy()
		df1['l1'] = df1.l
		df1['q1'] = df1.q
		df1['x1'] = df1.x
		df1['y1'] = df1.y
		df1.drop(columns=['l', 'q', 'x', 'y'], inplace=True)  # Origem
		df2 = self.centroides_lotes.copy()
		df2['l2'] = df2.l
		df2['q2'] = df2.q
		df2['x2'] = df2.x
		df2['y2'] = df2.y
		df2.drop(columns=['l', 'q', 'x', 'y'], inplace=True)  # Destino
		# Insere os dados no DataFrame de rotas
		self.rotas = self.rotas.merge(df1, on=['l1', 'q1'], how='left')
		self.rotas = self.rotas.merge(df2, on=['l2', 'q2'], how='left')

	def proc_id_quadras_e_lotes(self):
		"""Adiciona colunas de identificadores para quadras e lotes."""
		getid_q = np.vectorize(lambda q: self.ambiente.index_quadras[q])
		getid_l = np.vectorize(lambda q, l: self.ambiente.index_lotes[q][l])
		getid_r = np.vectorize(lambda r: self.ambiente.index_lotes['0000'][r])
		# DataFrame de vértices
		self.vertices['id_lote'] = getid_r(self.vertices.lote)
		# DataFrame de centroides de esquinas
		col_lote = self.ambiente.centroides.lote
		self.ambiente.centroides['id_l1'] = getid_r(col_lote)
		# DataFrame de ruas de destino
		quadras = self.ruas_destino.q2
		lotes = self.ruas_destino.l2
		ruas = self.ruas_destino.l1
		self.ruas_destino['id_rua'] = getid_r(ruas)
		self.ruas_destino['id_lote'] = getid_l(quadras, lotes)
		self.ruas_destino['id_quadra'] = getid_q(quadras)
		# DataFrame de rotas
		self.rotas['q1_id'] = getid_q(self.rotas.q1)
		self.rotas['l1_id'] = getid_l(self.rotas.q1, self.rotas.l1)
		self.rotas['q2_id'] = getid_q(self.rotas.q2)
		self.rotas['l2_id'] = getid_l(self.rotas.q2, self.rotas.l2)

	def gerar_arquivo_saida(self):
		print('Gerando arquivo 1-MOV.csv')
		with open(self.simul+'/1-MOV.csv', 'w') as file:
			stdout = sys.stdout
			sys.stdout = file
			self.saida_rotas()
			self.saida_trajetos()
			self.saida_periodos()
			self.saida_trajetos_fe()
			sys.stdout = stdout

	def saida_rotas(self):
		print('quantRotas, indexRotas e vetorRotas')
		print(len(self.rotas))
		print_arr_np(self.index_rotas)
		print_arr_np(self.vetor_rotas)

	def saida_trajetos(self):
		print('\nquantTrajetos e indexTrajetos')
		df = self.trajetos
		print(len(df))
		contar_rotas = lambda loc: len(loc.split(',')) - 1
		df['qtde_rotas'] = np.vectorize(contar_rotas)(df.locais)
		index_traj = np.hstack((0, df.qtde_rotas.values.flatten().cumsum()))
		df.drop(columns=['qtde_rotas'], inplace=True)
		print_arr_np(index_traj)

	def saida_periodos(self):
		print('\nindexPeriodos e vetorPeriodos')
		df = self.periodos
		print_arr_np(np.hstack((0, df.qtde.values.flatten().cumsum())))
		print(';'.join(list(df.periodos.values.flatten())))

	def saida_trajetos_fe(self):
		print('\nindexTrajetosFaixaEtaria')
		df = self.trajetos.groupby(['fe']).size()
		df = pd.DataFrame(df, columns=['qtde'])
		df.reset_index(inplace=True)
		index_traj_fe = np.hstack((0, df.qtde.values.flatten().cumsum()))
		print_arr_np(index_traj_fe)
