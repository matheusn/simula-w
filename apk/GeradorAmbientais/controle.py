#!/usr/bin/env python3

import sys
import banco
import numpy as np
import pandas as pd

def print_arr_np(arr, sep=';'):
	"""Imprime uma string formatada com os elementos de um vetor."""
	if type(arr) is not np.ndarray:
		arr = np.asarray(arr)
	arr.tofile(sys.stdout, sep=sep)
	print()  # Nova linha

class Controle:

	__slots__ = ['params', 'quadras_controle_amb', 'quadras_controle_bio',
				 'ambiente', 'quadras_vac', 'fe_vac', 'ciclos_vac',
				 'pont_est', 'dist_humanos', 'complemento', 'casos_dia',
				 'info_controles', 'pts_controles', 'pts_raios', 'vacinados',
				 'focos', 'lira', 'controle_por_quadra', 'simul']

	def __init__(self, params, ambiente, simul):
		print('Processando controles...')
		self.params = params
		self.simul = simul
		self.ambiente = ambiente
		self.ler_dados()
		self.proc_quadras_controle()
		self.proc_fe_vacinacao()
		self.proc_pontos_estrategicos()
		self.proc_complemento()
		self.proc_info_controles()
		self.proc_pontos_controles()
		self.proc_vacinados()
		self.proc_focos()
		self.proc_controle_por_quadra()

	def ler_dados(self):
		"""Acessa os dados de controle do banco de dados."""
		ambiente = self.params['Ambiente']
		self.quadras_controle_amb = banco.ler_quadras_controle_amb(ambiente)
		self.quadras_controle_bio = banco.ler_quadras_controle_bio(ambiente)
		self.quadras_vac = banco.ler_quadras_vacinacao(ambiente)
		self.fe_vac = banco.ler_fe_vacinacao(ambiente)
		self.ciclos_vac = banco.ler_ciclos_vacinacao(ambiente)
		self.pont_est = banco.ler_pontos_estrategicos(ambiente)
		self.dist_humanos = banco.ler_distribuicao_humanos(ambiente)
		self.info_controles = banco.ler_info_controles(ambiente)
		self.pts_controles = banco.ler_pontos_controles(ambiente)
		self.pts_raios = banco.ler_pontos_raios(ambiente)
		self.vacinados = banco.ler_vacinados(ambiente)
		self.lira = banco.ler_niveis_infestacao(ambiente)

	def proc_quadras_controle(self):
		getid_q = np.vectorize(lambda q: self.ambiente.index_quadras[q])
		df_amb = self.quadras_controle_amb
		if len(df_amb) > 0:
			df_amb.quadra = getid_q(df_amb.quadra)
		df_bio = self.quadras_controle_bio
		if len(df_bio) > 0:
			df_bio.quadra = getid_q(df_bio.quadra)
		df_vac = self.quadras_vac
		if len(df_vac) > 0:
			df_vac.quadra = getid_q(df_vac.quadra)

	def proc_fe_vacinacao(self):
		index_fe = {'Bebe':			0,	'Criança':	1,
					'Adolescente':	2,	'Jovem':	3,
					'Adulto':		4,	'Idoso':	5}
		getid_fe = np.vectorize(lambda fe: index_fe[fe])
		df_fe_vac = self.fe_vac
		if len(df_fe_vac) > 0:
			df_fe_vac.faixa_etaria = getid_q(df_fe_vac.faixa_etaria)

	def proc_pontos_estrategicos(self):
		getid_q = np.vectorize(lambda q: self.ambiente.index_quadras[q])
		getid_l = np.vectorize(lambda q, l: self.ambiente.index_lotes[q][l])
		self.pont_est.lote = getid_l(self.pont_est.quadra, self.pont_est.lote)
		self.pont_est.quadra = getid_q(self.pont_est.quadra)

	def proc_complemento(self):
		duracao_ano = int(self.params['DuraçãoAno'])
		# Agrupa os casos por ciclo
		dist = self.dist_humanos.groupby(['ciclo']).size()
		dist = pd.DataFrame(dist, columns=['qtde'])
		dist.reset_index(inplace=True)
		ciclos_com_caso = dist.ciclo.unique()
		for i in range(duracao_ano):
			if i not in ciclos_com_caso:  # Adiciona dias sem caso
				entry = {'ciclo': i, 'qtde': 0}
				dist.loc[len(dist)] = entry
		dist.sort_values(by=['ciclo'], inplace=True)
		dist.reset_index(inplace=True, drop=True)
		self.casos_dia = dist
		self.complemento = dist.qtde.values.flatten().cumsum()
		max_ = max(self.complemento)
		get_complemento = np.vectorize(lambda x: (max_ - x) / max_)
		to_string = np.vectorize(lambda f: '%.10f' % f)
		self.complemento = to_string(get_complemento(self.complemento))

	def proc_info_controles(self):
		df = self.info_controles
		# Adiciona o id das quadras
		getid_q = np.vectorize(lambda q: self.ambiente.index_quadras[q])
		df.quadra = getid_q(df.quadra)
		# Apenas a primeira letra simboliza o tipo de controle
		getchar = np.vectorize(lambda c: c[0])
		df.tipo_controle = getchar(df.tipo_controle)
		df.sort_values(by=['ciclo', 'tipo_controle', 'quadra'], inplace=True)
		df.reset_index(inplace=True, drop=True)

	def proc_pontos_controles(self):
		getid_q = np.vectorize(lambda q: self.ambiente.index_quadras[q])
		getid_l = np.vectorize(lambda q, l: self.ambiente.index_lotes[q][l])
		# Adiciona o id de quadras e lotes
		df = self.pts_controles
		df.lote = getid_l(df.quadra, df.lote)
		df.quadra = getid_q(df.quadra)
		df = self.pts_raios
		df.lote = getid_l(df.quadra, df.lote)
		df.quadra = getid_q(df.quadra)

	def proc_vacinados(self):
		getid_q = np.vectorize(lambda q: self.ambiente.index_quadras[q])
		getid_l = np.vectorize(lambda q, l: self.ambiente.index_lotes[q][l])
		df = self.vacinados
		if len(df) > 0:
			df.lote = getid_l(df.quadra, df.lote)
			df.quadra = getid_q(df.quadra)

	def proc_focos(self):
		self.focos = None
		frac = 0.15  # TODO mudar para um parâmetro externo
		pontos = self.ambiente.pontos.copy()
		quadras = pontos.quadra.unique()
		for quadra in quadras:  # Para cada quadra
			if quadra == '0000':
				# Não serão adicionados focos nas ruas
				continue
			# Sorteia uma fração de pontos que serão focos
			df_filt = pontos[pontos['quadra'] == quadra]
			pts_quadra = df_filt.sample(frac=frac)
			if self.focos is None:
				self.focos = pts_quadra
			else:
				self.focos = self.focos.append(pts_quadra)
		self.focos.sort_index(inplace=True)

	def proc_controle_por_quadra(self):
		df = self.lira.copy()
		map_niveis = {
			'Satisfatorio':		0,
			'Alerta':			1,
			'Risco':			2
		}
		df.faixa_2015_2 = df.faixa_2015_2.map(map_niveis)
		df.faixa_2015_3 = df.faixa_2015_3.map(map_niveis)
		df.faixa_2015_4 = df.faixa_2015_4.map(map_niveis)
		df.faixa_2016_1 = df.faixa_2016_1.map(map_niveis)
		df.faixa_2016_2 = df.faixa_2016_2.map(map_niveis)
		# Calcula os níveis de controle por quadra
		calc_nivel = lambda x, y: '%.2f' % (0.65 - 0.05 * (y - x))
		calc_nivel = np.vectorize(calc_nivel)
		df['nivel_0'] = calc_nivel(df.faixa_2015_2, df.faixa_2015_3)
		df['nivel_1'] = calc_nivel(df.faixa_2015_3, df.faixa_2015_4)
		df['nivel_2'] = calc_nivel(df.faixa_2015_4, df.faixa_2016_1)
		df['nivel_3'] = calc_nivel(df.faixa_2016_1, df.faixa_2016_2)
		cols = ['faixa_2015_2', 'faixa_2015_3', 'faixa_2015_4',
				'faixa_2016_1', 'faixa_2016_2']
		df.drop(columns=cols, inplace=True)
		# Adiciona um valor médio para entradas vazias
		quadras = self.ambiente.pontos.quadra.unique()
		for quadra in quadras:
			if quadra not in df.quadra.unique():
				entry = {
					'quadra':	quadra,
					'nivel_0':	0.65,
					'nivel_1':	0.65,
					'nivel_2':	0.65,
					'nivel_3':	0.65
				}
				df.loc[len(df)] = entry
		df.sort_values(by=['quadra'], inplace=True)
		df.reset_index(drop=True, inplace=True)
		self.controle_por_quadra = df

	def gerar_arquivo_saida(self):
		print('Gerando arquivo 2-CON.csv')
		with open(self.simul+'/2-CON.csv', 'w') as file:
			stdout = sys.stdout
			sys.stdout = file
			self.saida_quadras_vac()
			self.saida_fe_vac()
			self.saida_ciclos_vac()
			self.saida_controle_biologico()
			self.saida_controle_ambiental()
			self.saida_pontos_estrategicos()
			self.saida_focos()
			self.saida_controle_por_quadra()
			self.saida_complemento()
			self.saida_casos()
			self.saida_info_controles()
			self.saida_pontos_controles()
			self.saida_raios()
			self.saida_vacinacao()
			sys.stdout = stdout

	def gerar_arquivo_csv_focos(self):
		print('Gerando arquivo focos.csv')
		self.focos.to_csv(self.simul+'/focos.csv', sep=';', index_label='index')

	def saida_quadras_vac(self):
		print('quadVac. Quadras em que serao aplicadas a vacinacao')
		print(len(self.quadras_vac))
		print_arr_np(self.quadras_vac.quadra)

	def saida_fe_vac(self):
		print('\nfEVac, Faixas etarias que receberao vacinacao')
		print(len(self.fe_vac))
		print_arr_np(self.fe_vac.faixa_etaria)

	def saida_ciclos_vac(self):
		print('\ncicVac. Ciclos em que as vacinacoes serao executadas')
		print(len(self.ciclos_vac))
		print_arr_np(self.ciclos_vac.ciclo)

	def saida_controle_biologico(self):
		print('\nconBio. Quadras com controle biologico')
		print(len(self.quadras_controle_bio))
		print_arr_np(self.quadras_controle_bio.quadra)

	def saida_controle_ambiental(self):
		print('\nconAmb. Quadras com controle ambiental')
		print(len(self.quadras_controle_amb))
		print_arr_np(self.quadras_controle_amb.quadra)

	def saida_pontos_estrategicos(self):
		print('\npontEst. Lotes que sao pontos estrategicos')
		print(2 * len(self.pont_est))
		print_arr_np(self.pont_est.values.flatten())

	def saida_focos(self):
		print('\nindexFocos e vetorFocos')
		print_arr_np(self.gerar_indice_focos())
		print_arr_np(self.focos.index)

	def saida_controle_por_quadra(self):
		print('\nnLira e vetorControlePorQuadra')
		print(4)
		niveis = self.controle_por_quadra.drop(columns=['quadra'])
		print(';'.join([str(i) for i in niveis.values.flatten()]))

	def saida_complemento(self):
		print('\nComplemento')
		print(len(self.complemento))
		print(';'.join(self.complemento))

	def saida_casos(self):
		print('\nCasos')
		print(1 + len(self.casos_dia))
		print_arr_np(np.append([1], self.casos_dia.qtde.values.flatten()))

	def saida_info_controles(self):
		print('\ncontr. Informacoes sobre controles')
		print(len(self.info_controles))
		print(self.info_controles.to_csv(sep=';', header=False, index=False))

	def saida_pontos_controles(self):
		print('indContrPontos e contrPontos. Pontos com controles para mosquitos')
		indice = self.gerar_indice(self.pts_controles)
		print(len(indice))
		print_arr_np(indice)
		pontos = self.pts_controles.drop(columns=['id_controle'])
		print_arr_np(pontos.values.flatten())

	def saida_raios(self):
		print('\nindRaios e raios. Pontos de raio')
		indice = self.gerar_indice(self.pts_raios)
		print(len(indice))
		print_arr_np(indice)
		pontos = self.pts_raios.drop(columns=['id_controle'])
		print_arr_np(pontos.values.flatten())

	def saida_vacinacao(self):
		print('\nvacs. Humanos vacinados.')
		print(len(self.vacinados))
		print(self.vacinados.to_csv(sep=';', header=False, index=False))

	def gerar_indice(self, df):
		pontos = df.copy()
		indice = pontos.groupby(['id_controle']).size()
		indice = pd.DataFrame(indice, columns=['qtde'])
		indice.reset_index(inplace=True)
		# Adiciona entradas para controles sem pontos
		controles = indice.id_controle.unique()
		for i in range(len(self.info_controles)):
			if i not in controles:
				entry = {'id_controle' : i, 'qtde': 0}
				indice.loc[len(indice)] = entry
		indice.sort_values(by=['id_controle'], inplace=True)
		indice.reset_index(drop=True, inplace=True)
		return np.hstack((0, indice.qtde.values.flatten().cumsum()))

	def gerar_indice_focos(self):
		indice = self.focos.groupby(['quadra', 'lote']).size()
		indice = pd.DataFrame(indice, columns=['qtde'])
		indice.reset_index(inplace=True)
		# Adiciona uma entrada vazia no início de cada quadra
		last = len(indice)
		for quadra in self.ambiente.lotes.quadra.unique():
			entry = {'quadra': quadra, 'lote': '0', 'qtde': 0}
			indice.loc[last] = entry
			last += 1
			# Adiciona entradas vazias para lotes inexistentes
			filt = indice[indice['quadra'] == quadra]
			lotes_filt = filt.lote.unique()
			for lote in self.ambiente.index_lotes[quadra].keys():
				if lote not in lotes_filt:
					entry = {'quadra': quadra, 'lote': lote, 'qtde': 0}
					indice.loc[last] = entry
					last += 1
		# Reordena e reindexa o DataFrame
		indice.sort_values(by=['quadra', 'lote'], inplace=True)
		indice.reset_index(inplace=True, drop=True)
		return indice.qtde.values.flatten().cumsum()
