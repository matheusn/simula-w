#!/usr/bin/env python3

import sys
import banco

class Climatico:

	__slots__ = ['params', 'cli', 'simul']

	def __init__(self, params, simul):
		print('Processando sazonalidade...')
		self.simul = simul
		self.params = params
		self.ler_dados()

	def ler_dados(self):
		"""Acessa os dados climáticos do banco de dados."""
		ambiente = self.params['Ambiente']
		self.cli = banco.ler_climaticos(ambiente)

	def gerar_arquivo_saida(self):
		print('Gerando arquivo 3-CLI.csv')
		with open(self.simul+'/3-CLI.csv', 'w') as file:
			stdout = sys.stdout
			sys.stdout = file
			self.saida_cli()
			sys.stdout = stdout

	def saida_cli(self):
		print('cli')
		print(len(self.cli))
		print(self.cli.to_csv(sep=';', header=False, index=False))
